package com.appcraftz.spamember.ServerAgent;

/**
 * Created by appcraftz on 22/2/18.
 */

public class URLS {
    private static final String SERVER_URL = "http://13.228.218.242/~appcrf/development/Spa_New/webservices/";

    public static final String GALLERY_PATH = SERVER_URL + "images/";
    public static final String GET_PLANS = SERVER_URL + "get_plans.php";
    public static final String GET_MASSAGES = SERVER_URL + "get_massages.php";
    public static final String GET_MEMBERS = SERVER_URL + "get_members.php";
    public static final String GET_MASSAGE_HISTORY = SERVER_URL + "get_history.php";
    public static final String GET_REMAINING_COUPONS = SERVER_URL + "remaining_coupons.php";
    public static final String GET_PIECHART_DATA = SERVER_URL + "plan_piechart.php";
    public static final String LOGIN = SERVER_URL + "login.php";
    public static final String GET_CUSTOMER_INFO = SERVER_URL + "get_customer_info.php";
    public static final String ADD_DISCOUNT = SERVER_URL + "add_discount.php";
    public static final String DASHBOARD_COUNTS = SERVER_URL + "get_counts.php";
    public static final String ADMIN_LOGIN = SERVER_URL + "admin_login.php";
    public static final String GET_CUSTOMER_HISTORY = SERVER_URL + "get_customer_history.php";
    public static final String GET_APPROVAL_LIST = SERVER_URL + "get_approval_list.php";
    public static final String APPROVE_REQUEST = SERVER_URL + "approve_request.php";
    public static final String ADD_OFFER = SERVER_URL + "add_offer.php";
    public static final String GET_OFFERS = SERVER_URL + "get_offers.php";
    public static final String DELETE_OFFER = SERVER_URL + "delete_offer.php";
    public static final String FORGOT_PASSWORD = SERVER_URL + "forgot_password.php";
    public static final String CHANGE_PASSWORD = SERVER_URL + "change_pin.php";
    public static final String GET_SALON_MENUS = SERVER_URL + "get_salon_menus.php";
    public static final String GET_SALON_MENUS_NEW =SERVER_URL + "get_salon_menus_new.php";
}
