package com.appcraftz.spamember.ServerAgent;

/**
 * Created by appcraftz on 14/3/18.
 */

class KeyValueMap {
    String key,value;

    public KeyValueMap(String key,String value){
        this.key = key;
        this.value = value;
    }


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
