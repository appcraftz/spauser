package com.appcraftz.spamember.ServerAgent;

import android.content.Context;
import android.util.Log;

import com.appcraftz.spamember.Helpers.LogMaker;
import com.appcraftz.spamember.Helpers.SPHelper;
import com.appcraftz.spamember.Items.CustomerInfo;
import com.appcraftz.spamember.Items.DashboardCountItem;
import com.appcraftz.spamember.Items.MassageHistoryItem;
import com.appcraftz.spamember.Items.MassageItem;
import com.appcraftz.spamember.Items.MemberItem;
import com.appcraftz.spamember.Items.OfferItem;
import com.appcraftz.spamember.Items.PieChartItem;
import com.appcraftz.spamember.Items.PlanItem;
import com.appcraftz.spamember.Items.RemainingCouponItem;
import com.appcraftz.spamember.Items.SalonMenuItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.util.ArrayList;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

//import android.util.//Log;

public class ServerAgent {

    private OkHttpClient httpClient;
    private Context con;


    public ServerAgent(Context con)
    {
        httpClient = new OkHttpClient();
        this.con = con;

    }

    //TODO: add server methods here
    public String sendToServer(ArrayList<KeyValueMap> list, String serviceType)
    {
      // String response="";

        //Log.e("Url",serviceType);
        try
        {
            RequestBody formBody;
            FormBody.Builder builder= new FormBody.Builder();


          /*  builder.add("dbname", SPHelper.getSP(con,"dbname"));
            LogMaker.writeLog("KEY--","dbname");
            LogMaker.writeLog("VALUE--",SPHelper.getSP(con,"dbname"));
          */  for(KeyValueMap value:list)
            {
                builder.add(value.getKey(),value
                .getValue());

                LogMaker.writeLog("KEY--",value.getKey());
                LogMaker.writeLog("VALUE--",value.getValue());

            }
            formBody = builder.build();


            Request request = new Request.Builder()
                    .url(serviceType)
                    .post(formBody)
                    .build();


        Response response = httpClient.newCall(request).execute();
            String resp = response.body().string();

            Log.e("RESP",resp);

        return resp;

        }catch (Exception ex){
            ex.printStackTrace();
            if(ex.getClass() == SocketTimeoutException.class)
            {
                return "CONNECTION_ERROR";
            }
        }
        return "fail";

       // return response;
    }

    public ArrayList<PlanItem> getPlans() {
        ArrayList<KeyValueMap> maps = new ArrayList<>();
        maps.add(new KeyValueMap("data","data"));

        ArrayList<PlanItem> planItems = new ArrayList<>();
        try {
            JSONObject object = new JSONObject(sendToServer(maps,URLS.GET_PLANS));

            JSONArray jsonArray = object.getJSONArray("data");

            for (int i = 0 ; i<jsonArray.length();i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                PlanItem planItem = new PlanItem(jsonObject.getString("plan_id"),
                        jsonObject.getString("name"),
                        jsonObject.getString("amount"),
                        jsonObject.getString("coupon"));

                planItems.add(planItem);
            }
            return planItems;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public ArrayList<MassageItem> getMassages() {
        ArrayList<KeyValueMap> maps = new ArrayList<>();
        maps.add(new KeyValueMap("data","data"));

        ArrayList<MassageItem> massageItems = new ArrayList<>();
        try {
            JSONObject object = new JSONObject(sendToServer(maps,URLS.GET_MASSAGES));

            JSONArray jsonArray = object.getJSONArray("data");

            for (int i = 0 ; i<jsonArray.length();i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                MassageItem massageItem = new MassageItem(jsonObject.getString("massage_id"),
                        jsonObject.getString("name"),
                        jsonObject.getString("amount"),
                        jsonObject.getString("coupon"));

                massageItems.add(massageItem);
            }
            return massageItems;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }

    public ArrayList<MemberItem> getMembers() {

        ArrayList<KeyValueMap> maps = new ArrayList<>();
        maps.add(new KeyValueMap("purpose","getting_members"));

        ArrayList<MemberItem> memberItems = new ArrayList<>();

        try {
            JSONObject object = new JSONObject(sendToServer(maps,URLS.GET_MEMBERS));

            JSONArray jsonArray = object.getJSONArray("data");

            for (int i=0;i<jsonArray.length();i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                MemberItem memberItem = new MemberItem(
                        jsonObject.getString("member_id"),
                        jsonObject.getString("name"),
                        jsonObject.getString("number"),
                        jsonObject.getString("email"),
                        jsonObject.getString("age"),
                        jsonObject.getString("plan_id"),
                        jsonObject.getString("plan_name"),
                        jsonObject.getString("amount"),
                        jsonObject.getString("coupons"),
                        jsonObject.getString("address"),
                        jsonObject.getString("joining_date")
                );

                memberItems.add(memberItem);
            }
            return memberItems;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }

    public ArrayList<MassageHistoryItem> getMassageHistory(String type,String date) {
        ArrayList<KeyValueMap> maps = new ArrayList<>();
        maps.add(new KeyValueMap("purpose","getting_massageHistory"));
        if (type.equals("member"))
            maps.add(new KeyValueMap("type","1"));
        else
            maps.add(new KeyValueMap("type","0"));

        maps.add(new KeyValueMap("date",date));
        ArrayList<MassageHistoryItem> massageHistoryItems = new ArrayList<>();

        try {
            JSONObject object = new JSONObject(sendToServer(maps,URLS.GET_MASSAGE_HISTORY));


            JSONArray jsonArray = object.getJSONArray("data");

            for (int i=0;i<jsonArray.length();i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                /*
                *     "id":"32",
    "name":"Deva",
    "mobile":"9865676192",
    "email":"abc@gmail.com",
    "age":"25",
    "massage_type":"18",
    "massage_name":null,
    "amount":"1000",
    "address":"Nasik",
    "date":"10-3-2018"
    */
                MassageHistoryItem massageHistoryItem = new MassageHistoryItem(
                        jsonObject.getString("id"),
                        jsonObject.getString("name"),
                        jsonObject.getString("mobile"),
                        jsonObject.getString("email"),
                        jsonObject.getString("massage_type"),
                        jsonObject.getString("massage_name"),
                        jsonObject.getString("amount"),
                        jsonObject.getString("date"),
                        jsonObject.getString("therapist_name")
                );

                massageHistoryItem.setBalanceCoupons(jsonObject.getString("balance_coupons"));
                massageHistoryItems.add(massageHistoryItem);
            }
            return massageHistoryItems;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public RemainingCouponItem getRemainingCoupons(String number) {
        ArrayList<KeyValueMap> maps = new ArrayList<>();
        maps.add(new KeyValueMap("number",number));

        try {
            JSONObject object = new JSONObject(sendToServer(maps,URLS.GET_REMAINING_COUPONS));
            RemainingCouponItem remainingCouponItem = new RemainingCouponItem(
                    object.getString("total"),
                    object.getString("remaining"),
                    object.getString("used"));
            return remainingCouponItem;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }

    public ArrayList<PieChartItem> getPieChartItems() {
        ArrayList<KeyValueMap> maps = new ArrayList<>();
        maps.add(new KeyValueMap("purpose","getPiechartItems"));

        ArrayList<PieChartItem> pieChartItems = new ArrayList<>();

        try {
            JSONObject object = new JSONObject(sendToServer(maps,URLS.GET_PIECHART_DATA));

            JSONArray jsonArray = object.getJSONArray("data");
            for (int i =0; i<jsonArray.length();i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                Log.e("Count is",jsonObject.getString("count"));
                PieChartItem pieChartItem = new PieChartItem(jsonObject.getString("plan"),
                        Float.parseFloat(jsonObject.getString("count")));
                pieChartItems.add(pieChartItem);
            }
            Log.e("Size is",String.valueOf(pieChartItems.size()));
            return pieChartItems;

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }


    }

    public String login(String uname, String pwd) {

        ArrayList<KeyValueMap> maps = new ArrayList<>();
        maps.add(new KeyValueMap("username",uname));
        maps.add(new KeyValueMap("password",pwd));

        try {
            JSONObject object = new JSONObject(sendToServer(maps,URLS.LOGIN));

            return object.getString("data");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }

    public CustomerInfo getCustomerInfo(String username) {
        ArrayList<KeyValueMap> maps = new ArrayList<>();
        maps.add(new KeyValueMap("number",username));

        try {
            JSONObject object = new JSONObject(sendToServer(maps,URLS.GET_CUSTOMER_INFO));

            CustomerInfo customerInfo = new CustomerInfo(
                    object.getString("name"),
                    object.getString("date"),
                    object.getString("plan_name"),
                    object.getString("total"),
                    new RemainingCouponItem(object.getString("total"),
                            object.getString("remaining"),
                            object.getString("used"))
            );
            return customerInfo;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }

    public String addOffer(String disc, String discType) {

        ArrayList<KeyValueMap> maps = new ArrayList<>();
        maps.add(new KeyValueMap("discount",disc));
        maps.add(new KeyValueMap("discountType",discType));

        try {
            JSONObject object = new JSONObject(sendToServer(maps,URLS.ADD_DISCOUNT));

            return object.getString("data");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }


    }

    public DashboardCountItem getDashboardCounts() {
        ArrayList<KeyValueMap> maps = new ArrayList<>();
        maps.add(new KeyValueMap("purpose","dashboardCounts"));

        try {
            JSONObject object = new JSONObject(sendToServer(maps,URLS.DASHBOARD_COUNTS));

            DashboardCountItem countItem = new DashboardCountItem(
                    object.getString("members"),
                    object.getString("plans"),
                    object.getString("massages"),
                    object.getString("approvals")
            );
            return countItem;

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }


    }

    public String loginAdmin(String uname, String pwd) {

        ArrayList<KeyValueMap> maps = new ArrayList<>();
        maps.add(new KeyValueMap("username",uname));
        maps.add(new KeyValueMap("password",pwd));

        try {
            JSONObject object = new JSONObject(sendToServer(maps,URLS.ADMIN_LOGIN));

            return object.getString("data");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public ArrayList<MassageHistoryItem> getCustomerHistory(String username) {
        ArrayList<KeyValueMap> maps = new ArrayList<>();
        maps.add(new KeyValueMap("number",username));
        ArrayList<MassageHistoryItem> massageHistoryItems = new ArrayList<>();

        try {
            JSONObject object = new JSONObject(sendToServer(maps,URLS.GET_CUSTOMER_HISTORY));


            JSONArray jsonArray = object.getJSONArray("data");

            for (int i=0;i<jsonArray.length();i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                MassageHistoryItem massageHistoryItem = new MassageHistoryItem(
                        jsonObject.getString("id"),
                        jsonObject.getString("name"),
                        jsonObject.getString("mobile"),
                        jsonObject.getString("email"),
                        jsonObject.getString("massage_type"),
                        jsonObject.getString("massage_name"),
                        jsonObject.getString("amount"),
                        jsonObject.getString("date"),
                        jsonObject.getString("therapist_name")
                );

                massageHistoryItem.setBalanceCoupons(jsonObject.getString("balance_coupons"));
                massageHistoryItems.add(massageHistoryItem);
            }
            return massageHistoryItems;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public ArrayList<MemberItem> getApprovals(String type) {

        ArrayList<KeyValueMap> maps = new ArrayList<>();
        if (type.equals("member"))
            maps.add(new KeyValueMap("type","1"));
        else
            maps.add(new KeyValueMap("type","0"));

        ArrayList<MemberItem> memberItems = new ArrayList<>();

        try {
            JSONObject object = new JSONObject(sendToServer(maps,URLS.GET_APPROVAL_LIST));

            JSONArray jsonArray = object.getJSONArray("data");

            for (int i=0;i<jsonArray.length();i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                MemberItem memberItem = new MemberItem(
                        jsonObject.getString("member_id"),
                        jsonObject.getString("name"),
                        jsonObject.getString("number"),
                        jsonObject.getString("email"),
                        jsonObject.getString("age"),
                        jsonObject.getString("plan_id"),
                        jsonObject.getString("plan_name"),
                        jsonObject.getString("amount"),
                        jsonObject.getString("coupons"),
                        jsonObject.getString("address"),
                        jsonObject.getString("joining_date")
                );
                memberItem.setStatus(jsonObject.getString("status"));
                memberItem.setReason(jsonObject.getString("reason"));

                memberItems.add(memberItem);
            }
            return memberItems;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }

    public String approveRequets(String number, String type, String status) {
        ArrayList<KeyValueMap> maps = new ArrayList<>();
        if (type.equals("member"))
            maps.add(new KeyValueMap("type","1"));
        else
            maps.add(new KeyValueMap("type","0"));
        maps.add(new KeyValueMap("number",number));
        maps.add(new KeyValueMap("status",status));


        try {
            JSONObject object= new JSONObject(sendToServer(maps, URLS.APPROVE_REQUEST));

            return object.getString("data");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }

    public ArrayList<OfferItem> getOffers() {
        ArrayList<KeyValueMap> maps = new ArrayList<>();
        maps.add(new KeyValueMap("username", SPHelper.getSP(con,"username")));

        ArrayList<OfferItem> offerItems = new ArrayList<>();

        try {
            JSONObject jsonObject = new JSONObject(sendToServer(maps,URLS.GET_OFFERS));

            JSONArray jsonArray = jsonObject.getJSONArray("data");
            for (int i=0;i<jsonArray.length();i++){
                JSONObject object = jsonArray.getJSONObject(i);
                OfferItem offerItem = new OfferItem(object.getString("id"),object.getString("offer"));
                offerItems.add(offerItem);
            }
            return offerItems;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }

    public String deleteOffer(OfferItem offerItem) {
        ArrayList<KeyValueMap> maps = new ArrayList<>();
        maps.add(new KeyValueMap("offerId",offerItem.getId()));

        try {
            JSONObject object = new JSONObject(sendToServer(maps,URLS.DELETE_OFFER));
            return object.getString("data");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String forgotPassword(String num, String is_admin) {

        ArrayList<KeyValueMap> maps = new ArrayList<>();
        maps.add(new KeyValueMap("number",num));
        maps.add(new KeyValueMap("is_admin",is_admin));

        try {
            JSONObject object = new JSONObject(sendToServer(maps,URLS.FORGOT_PASSWORD));
            return object.getString("data");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }

    public String changePassword(String username, String newPass, String oldPass) {

        ArrayList<KeyValueMap> maps = new ArrayList<>();
        maps.add(new KeyValueMap("username",username));
        maps.add(new KeyValueMap("new_pin",newPass));
        maps.add(new KeyValueMap("old_pin",oldPass));

        try {
            JSONObject object = new JSONObject(sendToServer(maps,URLS.CHANGE_PASSWORD));
            return object.getString("data");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }


    }

    public ArrayList<MassageItem> getSalonMenus() {
        ArrayList<KeyValueMap> maps = new ArrayList<>();
        maps.add(new KeyValueMap("data","data"));

        ArrayList<MassageItem> massageItems = new ArrayList<>();
        try {
            JSONObject object = new JSONObject(sendToServer(maps,URLS.GET_SALON_MENUS));

            JSONArray jsonArray = object.getJSONArray("data");

            for (int i = 0 ; i<jsonArray.length();i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                MassageItem massageItem = new MassageItem(jsonObject.getString("massage_id"),
                        jsonObject.getString("name"),
                        jsonObject.getString("amount"),
                        jsonObject.getString("coupon"));

                massageItems.add(massageItem);
            }
            return massageItems;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }

    public ArrayList<SalonMenuItem> getSalonMenusNew() {

        ArrayList<KeyValueMap> maps = new ArrayList<>();

        maps.add(new KeyValueMap("purpose","salonItems"));

        try {
            ArrayList<SalonMenuItem> salonMenuItems = new ArrayList<>();
            JSONObject jsonObject = new JSONObject(sendToServer(maps,URLS.GET_SALON_MENUS_NEW));

            JSONArray jsonArray = jsonObject.getJSONArray("data");

            for (int i=0;i<jsonArray.length();i++){
                JSONObject object = jsonArray.getJSONObject(i);

                SalonMenuItem salonMenuItem = new SalonMenuItem(
                        object.getString("id"),
                        object.getString("main_category"),
                        object.getString("sub_category"),
                        object.getString("item"),
                        object.getString("cost")
                );

                salonMenuItems.add(salonMenuItem);

            }
            return salonMenuItems;

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }
}
