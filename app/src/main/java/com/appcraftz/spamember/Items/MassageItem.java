package com.appcraftz.spamember.Items;

import java.io.Serializable;

/**
 * Created by appcraftz on 14/3/18.
 */

public class MassageItem implements Serializable {
    String massageId,name,amount,coupon;

    public MassageItem(String massageId,String name,String amount,String coupon){
        this.massageId = massageId;
        this.name = name;
        this.amount = amount;
        this.coupon = coupon;
    }

    public String getMassageId() {
        return massageId;
    }

    public void setMassageId(String massageId) {
        this.massageId = massageId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCoupon() {
        return coupon;
    }

    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }
}
