package com.appcraftz.spamember.Items;

import java.io.Serializable;

public class SalonMenuItem implements Serializable{
    String id,mainCategory,subCategory,item,cost;
    int itemType;
    public SalonMenuItem(String id,String mainCategory,String subCategory,String item,String cost){
        this.id = id;
        this.mainCategory = mainCategory;
        this.subCategory = subCategory;
        this.item = item;
        this.cost = cost;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMainCategory() {
        return mainCategory;
    }

    public void setMainCategory(String mainCategory) {
        this.mainCategory = mainCategory;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public int getItemType() {
        return itemType;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }
}
