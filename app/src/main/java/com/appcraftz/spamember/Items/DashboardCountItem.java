package com.appcraftz.spamember.Items;

import java.io.Serializable;

/**
 * Created by appcraftz on 28/3/18.
 */

public class DashboardCountItem implements Serializable {
    String members,plans,massages,approvals;

    public DashboardCountItem(String members,String plans,String massages,String approvals){
        this.members = members;
        this.plans = plans;
        this.massages = massages;
        this.approvals = approvals;
    }

    public String getMembers() {
        return members;
    }

    public void setMembers(String members) {
        this.members = members;
    }

    public String getPlans() {
        return plans;
    }

    public void setPlans(String plans) {
        this.plans = plans;
    }

    public String getMassages() {
        return massages;
    }

    public void setMassages(String massages) {
        this.massages = massages;
    }

    public String getApprovals() {
        return approvals;
    }

    public void setApprovals(String approvals) {
        this.approvals = approvals;
    }
}
