package com.appcraftz.spamember.Items;

import java.io.Serializable;

/**
 * Created by appcraftz on 28/3/18.
 */

public class CustomerInfo implements Serializable {

    String name,date,plan,totalCoupons;
    RemainingCouponItem remainingCouponItem;

    public  CustomerInfo(String name,String date,String plan,String totalCoupons,RemainingCouponItem remainingCouponItem){
        this.name = name;
        this.date =date;
        this.plan = plan;
        this.totalCoupons =totalCoupons;
        this.remainingCouponItem = remainingCouponItem;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public String getTotalCoupons() {
        return totalCoupons;
    }

    public void setTotalCoupons(String totalCoupons) {
        this.totalCoupons = totalCoupons;
    }

    public RemainingCouponItem getRemainingCouponItem() {
        return remainingCouponItem;
    }

    public void setRemainingCouponItem(RemainingCouponItem remainingCouponItem) {
        this.remainingCouponItem = remainingCouponItem;
    }
}
