package com.appcraftz.spamember.Items;

import java.io.Serializable;

public class OfferItem implements Serializable {
    String id,offer;

    public OfferItem(String id, String offer){
        this.id = id;
        this.offer = offer;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOffer() {
        return offer;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }
}
