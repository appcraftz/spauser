package com.appcraftz.spamember.Items;

import java.io.Serializable;

/**
 * Created by appcraftz on 21/3/18.
 */

public class MassageHistoryItem implements Serializable {
    String id,name,number,email,massageId,massageName,amount,date,therapistName,balanceCoupons;

    public MassageHistoryItem(String id, String name, String number, String email, String massageId, String massageName, String amount, String date,String therapistName){
        this.id = id;
        this.name = name;
        this.number = number;
        this.email = email;
        this.massageId = massageId;
        this.massageName = massageName;
        this.amount = amount;
        this.date = date;
        this.therapistName = therapistName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMassageId() {
        return massageId;
    }

    public void setMassageId(String massageId) {
        this.massageId = massageId;
    }

    public String getMassageName() {
        return massageName;
    }

    public void setMassageName(String massageName) {
        this.massageName = massageName;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTherapistName() {
        return therapistName;
    }

    public void setTherapistName(String therapistName) {
        this.therapistName = therapistName;
    }

    public String getBalanceCoupons() {
        return balanceCoupons;
    }

    public void setBalanceCoupons(String balanceCoupons) {
        this.balanceCoupons = balanceCoupons;
    }
}
