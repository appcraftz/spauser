package com.appcraftz.spamember.Items;

import java.io.Serializable;

/**
 * Created by appcraftz on 22/3/18.
 */

public class PieChartItem implements Serializable {
    String label;
    Float value;

    public PieChartItem(String label,Float value){
        this.label = label;
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }
}

