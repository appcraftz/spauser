package com.appcraftz.spamember.Items;

import java.io.Serializable;

/**
 * Created by appcraftz on 14/3/18.
 */

public class PlanItem implements Serializable {

    String planId,name,amount,coupons;

    public PlanItem(String planId,String name,String amount,String coupons){

        this.planId = planId;
        this.name = name;
        this.amount = amount;
        this.coupons = coupons;
    }

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCoupons() {
        return coupons;
    }

    public void setCoupons(String coupons) {
        this.coupons = coupons;
    }
}
