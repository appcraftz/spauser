package com.appcraftz.spamember.Items;

import java.io.Serializable;

/**
 * Created by appcraftz on 20/3/18.
 */

public class MemberItem implements Serializable {

    String memberId,name,number,email,age,selectedPlanId,selectedPlanName,amountPaid,coupons,address,joiningDate;
    String status,reason;

    public MemberItem(String memberId, String name, String number, String email, String age, String selectedPlanId, String selectedPlanName, String amountPaid, String coupons, String address, String joiningDate){
        this.memberId = memberId;
        this.name = name;
        this.email =email;
        this.number = number;
        this.age = age;
        this.selectedPlanId = selectedPlanId;
        this.selectedPlanName = selectedPlanName;
        this.amountPaid =amountPaid;
        this.coupons = coupons;
        this.address = address;
        this.joiningDate = joiningDate;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getSelectedPlanId() {
        return selectedPlanId;
    }

    public void setSelectedPlanId(String selectedPlanId) {
        this.selectedPlanId = selectedPlanId;
    }

    public String getSelectedPlanName() {
        return selectedPlanName;
    }

    public void setSelectedPlanName(String selectedPlanName) {
        this.selectedPlanName = selectedPlanName;
    }

    public String getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(String amountPaid) {
        this.amountPaid = amountPaid;
    }

    public String getCoupons() {
        return coupons;
    }

    public void setCoupons(String coupons) {
        this.coupons = coupons;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getJoiningDate() {
        return joiningDate;
    }

    public void setJoiningDate(String joiningDate) {
        this.joiningDate = joiningDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
