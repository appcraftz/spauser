package com.appcraftz.spamember.Items;

import java.io.Serializable;

/**
 * Created by appcraftz on 22/3/18.
 */

public class RemainingCouponItem implements Serializable {
    String total,remaining,used;

    public RemainingCouponItem(String total,String remaining,String used){
        this.total = total;
        this.remaining = remaining;
        this.used = used;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getRemaining() {
        return remaining;
    }

    public void setRemaining(String remaining) {
        this.remaining = remaining;
    }

    public String getUsed() {
        return used;
    }

    public void setUsed(String used) {
        this.used = used;
    }
}
