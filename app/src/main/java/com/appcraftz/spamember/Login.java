package com.appcraftz.spamember;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.appcraftz.spamember.Activities.ForgotPassword;
import com.appcraftz.spamember.Activities.MemberDashboard;
import com.appcraftz.spamember.Helpers.MyProgressDialog;
import com.appcraftz.spamember.Helpers.NetworkStatus;
import com.appcraftz.spamember.Helpers.SPHelper;
import com.appcraftz.spamember.Helpers.ToastMaker;
import com.appcraftz.spamember.Helpers.Utils;
import com.appcraftz.spamember.ServerAgent.ServerAgent;

public class Login extends AppCompatActivity {

    ServerAgent agent;
    MyProgressDialog dialog;

    EditText username,password;
    String uname,pwd;
    TextView loginView;
    NetworkStatus networkStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.blue_login);
    }

    @Override
    protected void onStart() {
        super.onStart();

        setupViews(Login.this);


        findViewById(R.id.forgot).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Login.this, ForgotPassword.class));
            }
        });
        loginView.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("StaticFieldLeak")
            @Override
            public void onClick(View view) {

                if (!networkStatus.isOnline()){

                    if (Utils.isEmpty(username) || Utils.isEmpty(password)){
                        ToastMaker.emptyFields(Login.this);
                    }else if(password.getText().toString().length()<4){
                        ToastMaker.createToast(Login.this,"Please enter valid Password!");
                    }else {
                        new AsyncTask<Void, Void, String>() {

                            @Override
                            protected void onPreExecute() {
                                super.onPreExecute();
                                uname = username.getText().toString();
                                pwd = password.getText().toString();
                                dialog.show();
                            }

                            @Override
                            protected String doInBackground(Void... voids) {
                                return agent.login(uname,pwd);
                            }

                            @Override
                            protected void onPostExecute(String s) {
                                super.onPostExecute(s);

                                dialog.dismiss();
                                if (s!=null){

                                    if (s.equals("ENTERED")){
                                        ToastMaker.createToast(Login.this,"Login Successful");
                                        SPHelper.setSP(Login.this,"username",uname);
                                        SPHelper.setSP(Login.this,"isLoggedIn","true");
                                        startActivity(new Intent(Login.this, MemberDashboard.class));
                                        finish();
                                    }else {
                                        ToastMaker.createToast(Login.this,"Please enter valid credentials!");
                                    }
                                }else {
                                    ToastMaker.createToast(Login.this,"Unable to login please try again!");
                                }

                            }
                        }.execute(null,null,null);
                    }
                }else {
                    ToastMaker.noNetwork(Login.this);
                }

            }
        });
    }

    private void setupViews(Context context) {

        username = (EditText)findViewById(R.id.username);
        password = (EditText)findViewById(R.id.password);
        loginView = (TextView)findViewById(R.id.login);

        agent = new ServerAgent(context);
        dialog = new MyProgressDialog(context);
        networkStatus = NetworkStatus.getInstance(context);


    }


}
