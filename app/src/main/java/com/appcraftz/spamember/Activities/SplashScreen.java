package com.appcraftz.spamember.Activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.appcraftz.spamember.Helpers.SPHelper;
import com.appcraftz.spamember.Login;
import com.appcraftz.spamember.R;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                startActivity(new Intent(SplashScreen.this,MainDashboard.class));
                /*if (SPHelper.getSP(SplashScreen.this,"isLoggedIn").equals("true")){
                    startActivity(new Intent(SplashScreen.this,MemberDashboard.class));
                }else {
                    startActivity(new Intent(SplashScreen.this,MainDashboard.class));
                }*/
                finish();
            }
        },3000);
    }
}
