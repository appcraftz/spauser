package com.appcraftz.spamember.Activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.appcraftz.spamember.Adapters.ApprovalAdapter;
import com.appcraftz.spamember.Helpers.MyProgressDialog;
import com.appcraftz.spamember.Helpers.NetworkStatus;
import com.appcraftz.spamember.Helpers.ToastMaker;
import com.appcraftz.spamember.Interfaces.ApprovalClicked;
import com.appcraftz.spamember.Items.MemberItem;
import com.appcraftz.spamember.R;
import com.appcraftz.spamember.ServerAgent.ServerAgent;

import java.util.ArrayList;

public class Approvals extends AppCompatActivity {

    ServerAgent agent;
    NetworkStatus networkStatus;
    MyProgressDialog dialog;

    RecyclerView recyclerView;

    ApprovalAdapter adapter;
    String from = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approvals);

        if (getIntent().getExtras()!=null){
            from = getIntent().getExtras().getString("from");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        setupViews(Approvals.this);
        getData(from);
    }

    private void setupViews(Context context) {

        agent = new ServerAgent(context);
        dialog = new MyProgressDialog(context);
        networkStatus = NetworkStatus.getInstance(context);
        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);

    }


    @SuppressLint("StaticFieldLeak")
    private void getData(final String type) {
        new AsyncTask<Void, Void, ArrayList<MemberItem>>() {
            @Override
            protected ArrayList<MemberItem> doInBackground(Void... voids) {
                return agent.getApprovals(type);
            }

            @Override
            protected void onPostExecute(final ArrayList<MemberItem> memberItems) {
                super.onPostExecute(memberItems);
                if (memberItems!=null){
                    adapter = new ApprovalAdapter(Approvals.this,memberItems,type);
                    recyclerView.setAdapter(adapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(Approvals.this));
                    adapter.notifyDataSetChanged();

                    adapter.setOnApprovalClickedListener(new ApprovalClicked() {
                        @Override
                        public void onApprovalClicked(final MemberItem memberItem, final boolean isApproved) {

                            String msg = "";
                            if (isApproved) {
                                msg = "Are you sure to Approve this Request?";
                            }else {
                                msg = "Are you sure to Reject this Request?";
                            }


                            new AlertDialog.Builder(Approvals.this)
                                    .setMessage(msg)
                                    .setNegativeButton("Cancel",null)
                                    .setPositiveButton(isApproved ? "Approve" : "Reject", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {

                                            new AsyncTask<Void, Void, String>() {
                                                @Override
                                                protected String doInBackground(Void... voids) {

                                                    return agent.approveRequets(memberItem.getNumber(),type,isApproved?"2":"-1");
                                                }

                                                @Override
                                                protected void onPostExecute(String s) {
                                                    super.onPostExecute(s);

                                                    if (s!=null){
                                                        if (s.equals("Status changed successfully")){
                                                            ToastMaker.createToast(Approvals.this,"Status changed successfully");
                                                            memberItems.remove(memberItem);
                                                            adapter.notifyDataSetChanged();
                                                        }else {
                                                            ToastMaker.createToast(Approvals.this,s);
                                                        }
                                                    }
                                                }
                                            }.execute(null,null,null);
                                        }
                                    })
                                    .show();


                        }
                    });
                }

            }
        }.execute(null,null,null);
    }
}
