package com.appcraftz.spamember.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.appcraftz.spamember.Helpers.SPHelper;
import com.appcraftz.spamember.Login;
import com.appcraftz.spamember.R;

public class ChooseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose);
    }

    @Override
    protected void onStart() {
        super.onStart();
        (findViewById(R.id.spaServices)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Intent intent = new Intent(ChooseActivity.this, Plans.class);
                Intent intent = new Intent(ChooseActivity.this, NewMassages.class);
                Bundle bundle =new Bundle();
                bundle.putString("type","massages");
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        (findViewById(R.id.salonServices)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ChooseActivity.this, ChooseMainCategory.class);
                Bundle bundle =new Bundle();
                bundle.putString("type","salon");
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });


        findViewById(R.id.membership).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (SPHelper.getSP(ChooseActivity.this,"isLoggedIn").equals("true")){
                    startActivity(new Intent(ChooseActivity.this,MemberDashboard.class));
                }else {
                    startActivity(new Intent(ChooseActivity.this,Login.class));
                }
            }
        });

    }
}
