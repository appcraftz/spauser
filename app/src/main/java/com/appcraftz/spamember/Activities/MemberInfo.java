package com.appcraftz.spamember.Activities;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.appcraftz.spamember.Items.MemberItem;
import com.appcraftz.spamember.Items.RemainingCouponItem;
import com.appcraftz.spamember.R;
import com.appcraftz.spamember.ServerAgent.ServerAgent;

public class MemberInfo extends AppCompatActivity implements View.OnClickListener {

    MemberItem memberItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_info);

        memberItem = (MemberItem) getIntent().getSerializableExtra("memberItem");
    }

    @Override
    protected void onStart() {
        super.onStart();

        ((TextView)findViewById(R.id.memberName)).setText(memberItem.getName());
        ((TextView)findViewById(R.id.choosedPlan)).setText(memberItem.getSelectedPlanName());
        ((TextView)findViewById(R.id.joinedDate)).setText(memberItem.getJoiningDate());
        ((TextView)findViewById(R.id.remainingCoupons)).setText(memberItem.getCoupons());


        findViewById(R.id.call).setOnClickListener(this);
        findViewById(R.id.email).setOnClickListener(this);
        findViewById(R.id.history).setOnClickListener(this);

        getRemainingCoupons(MemberInfo.this);


    }

    @SuppressLint("StaticFieldLeak")
    private void getRemainingCoupons(final Context context) {
        new AsyncTask<Void, Void, RemainingCouponItem>() {
            @Override
            protected RemainingCouponItem doInBackground(Void... voids) {
                return new ServerAgent(context).getRemainingCoupons(memberItem.getNumber());
            }

            @Override
            protected void onPostExecute(RemainingCouponItem remainingCouponItem) {
                super.onPostExecute(remainingCouponItem);
                if (remainingCouponItem!=null){

                    ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
                    progressBar.setMax((int)Float.parseFloat(remainingCouponItem.getTotal()));
                    ObjectAnimator animation = ObjectAnimator.ofInt (progressBar, "progress", 0, (int)Float.parseFloat(remainingCouponItem.getUsed())); // see this max value coming back here, we animate towards that value
                    animation.setDuration (3000); //in milliseconds
                    animation.setInterpolator (new DecelerateInterpolator());
                    animation.start ();

                    ((TextView)findViewById(R.id.usedCount)).setText(remainingCouponItem.getUsed());
                    ((TextView)findViewById(R.id.remainingCoupons)).setText(remainingCouponItem.getRemaining());
                }
            }
        }.execute(null,null,null);

    }

    @SuppressLint("MissingPermission")
    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.call:
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + memberItem.getNumber()));
                startActivity(intent);


                break;
            case R.id.email:
                Uri uri = Uri.parse("mailto:" + memberItem.getEmail())
                        .buildUpon()
                        .appendQueryParameter("subject", "")
                        .appendQueryParameter("body", "")
                        .build();

                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, uri);
                (MemberInfo.this).startActivity(Intent.createChooser(emailIntent, "Choose"));
                break;
            case R.id.history:
                break;
        }
    }
}
