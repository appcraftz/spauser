package com.appcraftz.spamember.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.appcraftz.spamember.R;

public class TodaysHistory extends AppCompatActivity implements View.OnClickListener {

   /* TabLayout tabLayout;
    ViewPager pager;
    SimpleFragmentPagerAdapter adapter;*/

   String from = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todays_history);
       /* tabLayout = (TabLayout)findViewById(R.id.sliding_tabs);
        pager = (ViewPager)findViewById(R.id.pager);
*/
       if (getIntent().getExtras()!=null)
         from =getIntent().getExtras().getString("from");

    }

    @Override
    protected void onStart() {
        super.onStart();

        if (from.equals("approvals"))
            ((TextView)findViewById(R.id.headTag)).setText("Approvals");
        else
            ((TextView)findViewById(R.id.headTag)).setText("Collections");


        findViewById(R.id.cMembers).setOnClickListener(this);
        findViewById(R.id.cCustomers).setOnClickListener(this);
      /*  adapter = new SimpleFragmentPagerAdapter(this, getSupportFragmentManager());

        // Set the adapter onto the view pager
        pager.setAdapter(adapter);

        tabLayout.setupWithViewPager(pager);
*/
        /*for (int i = 0; i < tabLayout.getTabCount(); i++) {
            //noinspection ConstantConditions
            TextView tv = (TextView) LayoutInflater.from(TodaysHistory.this).inflate(R.layout.tab, null);
            tv.setText(adapter.getPageTitle(i));
            tabLayout.getTabAt(i).setCustomView(tv);
        }*/

    }

    @Override
    public void onClick(View view) {

        Intent intent = null;
        Bundle bundle = new Bundle();
        switch (view.getId()){

            case R.id.cMembers:
                if (from.equals("approvals"))
                    intent = new Intent(TodaysHistory.this,Approvals.class);
                else
                    intent = new Intent(TodaysHistory.this,Collections.class);
                bundle.putString("from","member");
                intent.putExtras(bundle);
                break;

            case R.id.cCustomers:
                if (from.equals("approvals"))
                    intent = new Intent(TodaysHistory.this,Approvals.class);
                else
                    intent = new Intent(TodaysHistory.this,Collections.class);
                bundle.putString("from","daily");
                intent.putExtras(bundle);
                break;
        }

        if (intent!=null)
            startActivity(intent);
    }
}
