package com.appcraftz.spamember.Activities;

import android.annotation.SuppressLint;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import com.appcraftz.spamember.R;
import com.squareup.picasso.Picasso;

public class FullscreenActivity extends AppCompatActivity {

    String imgPath = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_fullscreen);

        if (getIntent().getExtras()!=null){

            imgPath = getIntent().getExtras().getString("url");
            Picasso.with(FullscreenActivity.this).load(imgPath).into((ImageView)findViewById(R.id.fullscreen_content));
        }

    }

}
