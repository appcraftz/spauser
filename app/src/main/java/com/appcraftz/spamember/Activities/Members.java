package com.appcraftz.spamember.Activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.appcraftz.spamember.Adapters.MemberAdapter;
import com.appcraftz.spamember.Helpers.MyProgressDialog;
import com.appcraftz.spamember.Helpers.NetworkStatus;
import com.appcraftz.spamember.Interfaces.MemberItemClicked;
import com.appcraftz.spamember.Items.MemberItem;
import com.appcraftz.spamember.R;
import com.appcraftz.spamember.ServerAgent.ServerAgent;

import java.util.ArrayList;

public class Members extends AppCompatActivity {

    RecyclerView recyclerView;
    ServerAgent agent;
    MyProgressDialog myProgressDialog;
    NetworkStatus networkStatus;
    ArrayList<MemberItem> memberItems;
    MemberAdapter memberAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_members);
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    protected void onStart() {
        super.onStart();
        setupViews(Members.this);

        new AsyncTask<Void, Void, ArrayList<MemberItem>>() {
            @Override
            protected ArrayList<MemberItem> doInBackground(Void... voids) {
                return agent.getMembers();
            }

            @Override
            protected void onPostExecute(ArrayList<MemberItem> imemberItems) {
                super.onPostExecute(imemberItems);
                if (imemberItems!=null){

                    memberItems = imemberItems;

                    memberAdapter = new MemberAdapter(Members.this,memberItems);
                    recyclerView.setAdapter(memberAdapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(Members.this));
                    memberAdapter.notifyDataSetChanged();
                    memberAdapter.setOnMemberItemClickedListener(new MemberItemClicked() {
                        @Override
                        public void onMemberItemClicked(MemberItem memberItem) {
                            Intent intent = new Intent(Members.this,MemberInfo.class);
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("memberItem",memberItem);
                            intent.putExtras(bundle);
                            startActivity(intent);
                        }
                    });
                }

            }
        }.execute(null,null,null);
    }

    private void setupViews(Context context) {
        agent = new ServerAgent(context);
        myProgressDialog = new MyProgressDialog(context);
        networkStatus = NetworkStatus.getInstance(context);

        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
    }
}
