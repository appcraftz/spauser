package com.appcraftz.spamember.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.appcraftz.spamember.R;

public class AboutUs extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
    }

    @Override
    protected void onStart() {
        super.onStart();

        findViewById(R.id.callUs).setOnClickListener(this);
        findViewById(R.id.messageUs).setOnClickListener(this);
        findViewById(R.id.locateUs).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.callUs:
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:9767724825"));
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(intent);
                break;

            case R.id.messageUs:
                Uri uri = Uri.parse("smsto:9767724825");
                Intent it = new Intent(Intent.ACTION_SENDTO, uri);
                it.putExtra("sms_body", "");
                startActivity(it);
                break;

            case R.id.locateUs:
                Uri mapUri = Uri.parse("geo:0,0?q=19.984679,73.774568(CAELUM SPA & WELLNESS PVT.LTD)");
                //    Log.e("URI",""+mapUri);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, mapUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
                break;
        }
    }
}
