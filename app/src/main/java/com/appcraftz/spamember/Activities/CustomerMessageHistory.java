package com.appcraftz.spamember.Activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.appcraftz.spamember.Adapters.MassageHistoryAdapter;
import com.appcraftz.spamember.Helpers.MyProgressDialog;
import com.appcraftz.spamember.Helpers.NetworkStatus;
import com.appcraftz.spamember.Helpers.SPHelper;
import com.appcraftz.spamember.Items.MassageHistoryItem;
import com.appcraftz.spamember.R;
import com.appcraftz.spamember.ServerAgent.ServerAgent;

import java.util.ArrayList;

public class CustomerMessageHistory extends AppCompatActivity {
ServerAgent agent;
MyProgressDialog dialog;
NetworkStatus networkStatus;
RecyclerView recyclerView;
MassageHistoryAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mem_history);
    }

    @Override
    protected void onStart() {
        super.onStart();

        setupViews(CustomerMessageHistory.this);
        getData(CustomerMessageHistory.this);

    }

    @SuppressLint("StaticFieldLeak")
    private void getData(final Context context) {
        new AsyncTask<Void, Void, ArrayList<MassageHistoryItem>>() {
            @Override
            protected ArrayList<MassageHistoryItem> doInBackground(Void... voids) {
                return agent.getCustomerHistory(SPHelper.getSP(context,"username"));
            }

            @Override
            protected void onPostExecute(ArrayList<MassageHistoryItem> massageHistoryItems) {
                super.onPostExecute(massageHistoryItems);
                if (massageHistoryItems!=null){


                    adapter = new MassageHistoryAdapter(context,massageHistoryItems,"member");
                    recyclerView.setAdapter(adapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(context));
                    adapter.notifyDataSetChanged();

                }
            }
        }.execute(null,null,null);
    }

    private void setupViews(Context context) {

        agent = new ServerAgent(context);
        dialog = new MyProgressDialog(context);
        networkStatus = NetworkStatus.getInstance(context);

        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);

    }
}
