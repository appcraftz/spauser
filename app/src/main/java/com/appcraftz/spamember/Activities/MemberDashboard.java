package com.appcraftz.spamember.Activities;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.appcraftz.spamember.Helpers.MyProgressDialog;
import com.appcraftz.spamember.Helpers.NetworkStatus;
import com.appcraftz.spamember.Helpers.SPHelper;
import com.appcraftz.spamember.Items.CustomerInfo;
import com.appcraftz.spamember.Login;
import com.appcraftz.spamember.R;
import com.appcraftz.spamember.ServerAgent.ServerAgent;

public class MemberDashboard extends AppCompatActivity {

    ServerAgent agent;
    NetworkStatus networkStatus;
    MyProgressDialog dialog;
    TextView name,usedCount,remainingCount,date,plan,totalCoupons;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mem_dash_alternate);
    }

    @Override
    protected void onStart() {
        super.onStart();
        setupViews(MemberDashboard.this);
        getCustomerData(MemberDashboard.this);

        findViewById(R.id.logoutC).setOnClickListener(new View.OnClickListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(MemberDashboard.this)
                        .setMessage("Are you sure to Logout?")
                        .setPositiveButton("Logout", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                SPHelper.setSP(MemberDashboard.this,"isLoggedIn","false");
                              //  startActivity(new Intent(MemberDashboard.this, MainDashboard.class));
                                finish();
                            }
                        })
                        .setNegativeButton("Cancel",null)
                        .show();
            }
        });

        findViewById(R.id.call).setOnClickListener(new View.OnClickListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel: 097677 24825"));
                startActivity(intent);
            }
        });
        findViewById(R.id.email).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("mailto: md@caelumspa.com")
                        .buildUpon()
                        .appendQueryParameter("subject", "")
                        .appendQueryParameter("body", "")
                        .build();

                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, uri);
                startActivity(Intent.createChooser(emailIntent, "Choose"));
            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    private void getCustomerData(final Context context) {
        new AsyncTask<Void, Void, CustomerInfo>() {
            @Override
            protected CustomerInfo doInBackground(Void... voids) {
                return agent.getCustomerInfo(SPHelper.getSP(context,"username"));
            }

            @Override
            protected void onPostExecute(CustomerInfo customerInfo) {
                super.onPostExecute(customerInfo);
                if (customerInfo!=null){
                    name.setText(customerInfo.getName());
                    plan.setText("Selected Plan : "+customerInfo.getPlan());
                    date.setText(customerInfo.getDate());
                    usedCount.setText(customerInfo.getRemainingCouponItem().getUsed());
                    remainingCount.setText(customerInfo.getRemainingCouponItem().getRemaining());
                    totalCoupons.setText(customerInfo.getRemainingCouponItem().getTotal());


                    ProgressBar progressBar = (ProgressBar) findViewById(R.id.usedProgressbar);
                    progressBar.setMax((int)Float.parseFloat(customerInfo.getRemainingCouponItem().getTotal()));
                    ObjectAnimator animation = ObjectAnimator.ofInt (progressBar, "progress", 0, (int)Float.parseFloat(customerInfo.getRemainingCouponItem().getUsed())); // see this max value coming back here, we animate towards that value
                    animation.setDuration (3000); //in milliseconds
                    animation.setInterpolator (new DecelerateInterpolator());
                    animation.start ();

                    ProgressBar progressBar1 = (ProgressBar) findViewById(R.id.remainingProgressbar);
                    progressBar1.setMax((int)Float.parseFloat(customerInfo.getRemainingCouponItem().getTotal()));
                    ObjectAnimator animation1 = ObjectAnimator.ofInt (progressBar1, "progress", 0, (int)Float.parseFloat(customerInfo.getRemainingCouponItem().getRemaining())); // see this max value coming back here, we animate towards that value
                    animation1.setDuration (3000); //in milliseconds
                    animation1.setInterpolator (new DecelerateInterpolator());
                    animation1.start ();
                }
            }
        }.execute(null,null,null);
    }

    private void setupViews(Context context) {

        agent = new ServerAgent(context);
        networkStatus = NetworkStatus.getInstance(context);
        dialog = new MyProgressDialog(context);

        name = (TextView)findViewById(R.id.mName);
        usedCount = (TextView)findViewById(R.id.custUsedCount);
        remainingCount = (TextView)findViewById(R.id.custRemainingCount);
        date = (TextView)findViewById(R.id.dateJoined);
        plan = (TextView)findViewById(R.id.mPlan);
        totalCoupons = (TextView)findViewById(R.id.totalCoupons);

        findViewById(R.id.viewHistory).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MemberDashboard.this, CustomerMessageHistory.class));
            }
        });

        findViewById(R.id.changePIN).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MemberDashboard.this, ChangePassword.class));
            }
        });

        /*findViewById(R.id.logout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SPHelper.setSP(MemberDashboard.this,"isLoggedIn","false");
                startActivity(new Intent(MemberDashboard.this, Login.class));
                finish();
            }
        });*/
    }
}
