package com.appcraftz.spamember.Activities;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.appcraftz.spamember.R;
import com.appcraftz.spamember.ServerAgent.URLS;
import com.squareup.picasso.Picasso;

public class Gallery extends AppCompatActivity implements View.OnClickListener {

    String img1 = URLS.GALLERY_PATH+"img1.jpg";
    String img2 = URLS.GALLERY_PATH+"img2.jpg";
    String img3 = URLS.GALLERY_PATH+"img3.jpg";
    String img4 = URLS.GALLERY_PATH+"img4.jpg";
    String img5 = URLS.GALLERY_PATH+"img5.jpg";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
    }

    @Override
    protected void onStart() {
        super.onStart();



        //Log.e("Img Url",URLS.GALLERY_PATH+"img1.jpg");
        Picasso.with(Gallery.this).load(img1).into((ImageView)findViewById(R.id.img1));
        Picasso.with(Gallery.this).load(img2).into((ImageView)findViewById(R.id.img2));
        Picasso.with(Gallery.this).load(img3).into((ImageView)findViewById(R.id.img3));
        Picasso.with(Gallery.this).load(img4).into((ImageView)findViewById(R.id.img4));
        Picasso.with(Gallery.this).load(img5).into((ImageView)findViewById(R.id.img5));


        findViewById(R.id.img1).setOnClickListener(this);
        findViewById(R.id.img2).setOnClickListener(this);
        findViewById(R.id.img3).setOnClickListener(this);
        findViewById(R.id.img4).setOnClickListener(this);
        findViewById(R.id.img5).setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {

        String url = "";
        switch (view.getId()){
            case R.id.img1:
                url = img1;
            break;

            case R.id.img2:
                url = img2;
                break;


            case R.id.img3:
                url = img3;
                break;


            case R.id.img4:
                url = img4;
                break;


            case R.id.img5:
                url = img5;
                break;
        }

        /*View alertView = LayoutInflater.from(Gallery.this).inflate(R.layout.galleryview,null);
        AlertDialog.Builder alert = new AlertDialog.Builder(Gallery.this);
        alert.setView(alertView);
        ImageView im = (ImageView)alertView.findViewById(R.id.imageView);
        Picasso.with(Gallery.this).load(url).into(im);

        alert.setPositiveButton("OK",null);
        alert.show();*/
        Intent intent = new Intent(Gallery.this,FullscreenActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("url",url);
        intent.putExtras(bundle);
        startActivity(intent);

    }
}
