package com.appcraftz.spamember.Activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.appcraftz.spamember.Helpers.SPHelper;
import com.appcraftz.spamember.Items.DashboardCountItem;
import com.appcraftz.spamember.R;
import com.appcraftz.spamember.ServerAgent.ServerAgent;
import com.github.mikephil.charting.charts.PieChart;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    ServerAgent agent;
    PieChart pieChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
    }

    @Override
    protected void onStart() {
        super.onStart();

        agent = new ServerAgent(MainActivity.this);
       // pieChart = (PieChart)findViewById(R.id.roomPieChart);
        (findViewById(R.id.plans)).setOnClickListener(this);
        (findViewById(R.id.massages)).setOnClickListener(this);
        (findViewById(R.id.mem)).setOnClickListener(this);
        (findViewById(R.id.coll)).setOnClickListener(this);
        (findViewById(R.id.logout)).setOnClickListener(this);
        (findViewById(R.id.addOffer)).setOnClickListener(this);
        (findViewById(R.id.approvals)).setOnClickListener(this);


        getCounts();
       // setupPlansChart(MainActivity.this);
    }

    @SuppressLint("StaticFieldLeak")
    private void getCounts() {

        new AsyncTask<Void, Void, DashboardCountItem>() {
            @Override
            protected DashboardCountItem doInBackground(Void... voids) {
                return agent.getDashboardCounts();
            }

            @Override
            protected void onPostExecute(DashboardCountItem dashboardCountItem) {
                super.onPostExecute(dashboardCountItem);
                if (dashboardCountItem!=null){
                    ((TextView)findViewById(R.id.btn1)).setText("Total Members : "+dashboardCountItem.getMembers());
                    ((TextView)findViewById(R.id.btn2)).setText("Plans : "+dashboardCountItem.getPlans());
                    ((TextView)findViewById(R.id.btn3)).setText("Massages : "+dashboardCountItem.getMassages());
                    ((TextView)findViewById(R.id.btn4)).setText("Approvals : "+dashboardCountItem.getApprovals());
                }
            }
        }.execute(null,null,null);
    }

    /*  @SuppressLint("StaticFieldLeak")
      private void setupPlansChart(Context context) {

          new AsyncTask<Void, Void, ArrayList<PieChartItem>>() {
              @Override
              protected ArrayList<PieChartItem> doInBackground(Void... voids) {
                  return agent.getPieChartItems();
              }

              @Override
              protected void onPostExecute(ArrayList<PieChartItem> pieChartItems) {
                  super.onPostExecute(pieChartItems);


                  if (pieChartItems!=null){
                      List<Entry> yvalues = new ArrayList<Entry>();
                      ArrayList<String> xvalues = new ArrayList<>();

                      for (int i= 0 ; i<pieChartItems.size();i++){
                          try {
                              Log.e("yvalues ",String.valueOf(pieChartItems.get(i).getValue()));
                              if (pieChartItems.get(i).getValue() >0) {
                                  yvalues.add(new Entry(pieChartItems.get(i).getValue(), i));
                                  xvalues.add(pieChartItems.get(i).getLabel());
                              }
                          }catch (Exception ex){
                          }
                      }

                      Log.e("yvalues size",String.valueOf(yvalues.size()));

                      PieDataSet dataSet = new PieDataSet(yvalues, "Plans Used");
                      dataSet.setColors(new int[]	{R.color.priorityLow,R.color.priorityMedium,R.color.darkyellow,R.color.red,R.color.sinch_grey,R.color.purple,R.color.black},MainActivity.this);

                      PieData data = new PieData(xvalues,dataSet);

                      data.setValueFormatter(new PercentFormatter());

                      pieChart.setData(data);
                      pieChart.setDrawHoleEnabled(true);
                      pieChart.setTransparentCircleRadius(25f);
                      //roomsBookedPieChart.setEntryLabelTextSize(.0f);
                      //roomsBookedPieChart.setEntryLabelColor(Color.BLACK);

                      pieChart.setHoleRadius(60f);
                      pieChart.getLegend().setEnabled(false);
                      //Description description = new Description();
                      //description.setText("");
                      //roomsBookedPieChart.setDescription(description);
                      //dataSet.setColors(ColorTemplate.LIBERTY_COLORS);
                      dataSet.setColors(ColorTemplate.COLORFUL_COLORS);
                      data.setValueTextSize(18f);
                      data.setValueTextColor(Color.BLACK);
                      pieChart.animateXY(1400, 1400);
                  }else {
                      Log.e("Hello ","Im here");
                  }

              }
          }.execute(null,null,null);

      }
  */
    @Override
    public void onClick(View view) {
        Intent intent;
        Bundle bundle;
        switch (view.getId()){
            case R.id.plans:
                intent = new Intent(MainActivity.this,Plans.class);
                bundle = new Bundle();
                bundle.putString("type","plans");
                intent.putExtras(bundle);
                startActivity(intent);
                break;

            case R.id.massages:
                intent = new Intent(MainActivity.this,Plans.class);
                bundle = new Bundle();
                bundle.putString("type","massages");
                intent.putExtras(bundle);
                startActivity(intent);
                break;

            case R.id.mem:
                intent = new Intent(MainActivity.this,Members.class);
                startActivity(intent);
                break;

            case R.id.coll:
                intent = new Intent(MainActivity.this,TodaysHistory.class);
                bundle = new Bundle();
                bundle.putString("from","collections");
                intent.putExtras(bundle);

                startActivity(intent);
                break;

            case R.id.approvals:
                intent = new Intent(MainActivity.this,TodaysHistory.class);
                bundle = new Bundle();
                bundle.putString("from","approvals");
                intent.putExtras(bundle);

                startActivity(intent);
                break;


            case R.id.addOffer:
                intent = new Intent(MainActivity.this,AddOffer.class);
                startActivity(intent);
                break;

            case R.id.logout:
                new AlertDialog.Builder(MainActivity.this)
                        .setMessage("Are you sure to Logout")
                        .setNegativeButton("Cancel",null)
                        .setPositiveButton("Logout", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                SPHelper.setSP(MainActivity.this,"isLoggedIn","false");
                                Intent intent2 = new Intent(MainActivity.this,AdminLogin.class);
                                startActivity(intent2);
                                finish();
                            }
                        })
                        .show();
                break;
        }
    }
}
