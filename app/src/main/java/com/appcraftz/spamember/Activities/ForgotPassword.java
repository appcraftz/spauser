package com.appcraftz.spamember.Activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.appcraftz.spamember.Helpers.MyProgressDialog;
import com.appcraftz.spamember.Helpers.ToastMaker;
import com.appcraftz.spamember.R;
import com.appcraftz.spamember.ServerAgent.ServerAgent;

public class ForgotPassword extends AppCompatActivity {
    EditText number;
    Button submit;
    ServerAgent agent;
    MyProgressDialog dialog;

    String num;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_alternate);
    }

    @Override
    protected void onStart() {
        super.onStart();

        setupViews(ForgotPassword.this);

        submit.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("StaticFieldLeak")
            @Override
            public void onClick(View view) {
                if (number.getText().toString().trim().equals("")){
                    ToastMaker.emptyFields(ForgotPassword.this);
                }else if (number.getText().toString().trim().length()<10){
                    ToastMaker.createToast(ForgotPassword.this,"Please enter valid Number!");
                }else {
                    new AsyncTask<Void, Void, String>() {

                        @Override
                        protected void onPreExecute() {
                            super.onPreExecute();
                            num = number.getText().toString();
                            dialog.show();
                        }

                        @Override
                        protected String doInBackground(Void... voids) {
                            return agent.forgotPassword(num,"0");
                        }

                        @Override
                        protected void onPostExecute(String s) {
                            super.onPostExecute(s);
                            dialog.dismiss();
                            if (s!=null){
                                if (s.equals("SUCCESS")){
                                    number.setText("");
                                    new AlertDialog.Builder(ForgotPassword.this)
                                            .setMessage("Your password has been sent to your registered mobile number.")
                                            .setPositiveButton("OK",null)
                                            .show();
                                }else if(s.equals("NO RECORD FOUND")){

                                    new AlertDialog.Builder(ForgotPassword.this)
                                            .setMessage("No such Member record found!")
                                            .setPositiveButton("OK",null)
                                            .show();
                                }
                            }else {
                                ToastMaker.createToast(ForgotPassword.this,"Unable to reset password!");
                            }
                        }
                    }.execute(null,null,null);
                }
            }
        });
    }

    private void setupViews(Context context) {
        number = (EditText)findViewById(R.id.number);
        submit = (Button) findViewById(R.id.submit);

        agent = new ServerAgent(context);
        dialog = new MyProgressDialog(context);
    }

}
