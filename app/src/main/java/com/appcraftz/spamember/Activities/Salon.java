package com.appcraftz.spamember.Activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.appcraftz.spamember.Adapters.SalonAdapter;
import com.appcraftz.spamember.Helpers.MyProgressDialog;
import com.appcraftz.spamember.Helpers.ToastMaker;
import com.appcraftz.spamember.Items.SalonMenuItem;
import com.appcraftz.spamember.R;
import com.appcraftz.spamember.ServerAgent.ServerAgent;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class Salon extends AppCompatActivity implements View.OnClickListener {

    ServerAgent agent;
    MyProgressDialog dialog;
    Spinner mainSpinner,subSpinner;
    ArrayList<SalonMenuItem> salonMenuItems;
    RecyclerView recyclerView;

    String type = "";
    ArrayList<String> mainMenus,subMenus;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salon);

        if (getIntent()!=null){
            type = getIntent().getExtras().getString("type");
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        setupViews(Salon.this);

        getSalonItems(Salon.this);



    }

    @SuppressLint("StaticFieldLeak")
    private void getSalonItems(final Context context) {
        new AsyncTask<Void, Void, ArrayList<SalonMenuItem>>() {
            @Override
            protected ArrayList<SalonMenuItem> doInBackground(Void... voids) {
                return agent.getSalonMenusNew();
            }

            @Override
            protected void onPostExecute(ArrayList<SalonMenuItem> isalonMenuItems) {
                super.onPostExecute(isalonMenuItems);

                if(isalonMenuItems!=null) {

                    salonMenuItems = isalonMenuItems;

                    mainMenus = new ArrayList<>();
                    subMenus = new ArrayList<>();

                    for (SalonMenuItem menuItem : salonMenuItems) {

                        if (!mainMenus.contains(menuItem.getMainCategory())) {
                            mainMenus.add(menuItem.getMainCategory());
                        }
                    }

                    if (type.equals("MEN"))
                        loadItems(0);
                    else if (type.equals("WOMEN"))
                        loadItems(1);
                    else if (type.equals("COMMON"))
                        loadItems(2);
/*
                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(Salon.this, android.R.layout.simple_spinner_item, mainMenus);
                    arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    mainSpinner.setAdapter(arrayAdapter);
*/

                    //mainMenus = salonMenuItems.stream().distinct().collect(Collectors.toList());
                }else {
                    ToastMaker.createToast(context,"Please try again!");
                }

            }
        }.execute(null,null,null);
    }

    private void setupViews(Context context) {

        agent = new ServerAgent(context);
        dialog = new MyProgressDialog(context);

        mainSpinner = (Spinner)findViewById(R.id.mainSpinner);
        subSpinner = (Spinner)findViewById(R.id.subSpinner);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
/*
        (findViewById(R.id.men)).setOnClickListener(this);
        (findViewById(R.id.women)).setOnClickListener(this);
        (findViewById(R.id.common)).setOnClickListener(this);*/

    }


    public void loadSalonUpdateSpinner(){
/*
  if(isalonMenuItems!=null) {

                    salonMenuItems = isalonMenuItems;

                    mainMenus = new ArrayList<>();
                    subMenus = new ArrayList<>();

                    for (SalonMenuItem menuItem : salonMenuItems) {

                        if (!mainMenus.contains(menuItem.getMainCategory())) {
                            mainMenus.add(menuItem.getMainCategory());
                        }
                    }

                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(Salon.this, android.R.layout.simple_spinner_item, mainMenus);
                    arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    mainSpinner.setAdapter(arrayAdapter);

                    mainSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                            subMenus.clear();
                            for (SalonMenuItem menuItem : salonMenuItems) {
                                if (mainMenus.get(i).equals(menuItem.getMainCategory())) {

                                    if (!subMenus.contains(menuItem.getSubCategory())) {
                                        subMenus.add(menuItem.getSubCategory());
                                    }

                                }
                            }


                            ArrayAdapter<String> subAdapter = new ArrayAdapter<String>(Salon.this, android.R.layout.simple_spinner_item, subMenus);
                            subAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            subSpinner.setAdapter(subAdapter);


                            subSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                                    ArrayList<SalonMenuItem> item = new ArrayList<>();

                                    for (SalonMenuItem menuItem : salonMenuItems) {
                                        if (mainSpinner.getSelectedItem().toString().equals(menuItem.getMainCategory())) {

                                            if (subSpinner.getSelectedItem().toString().equals(menuItem.getSubCategory())) {
                                                item.add(menuItem);
                                            }
                                        }
                                    }

                                    SalonAdapter salonAdapter = new SalonAdapter(Salon.this, item);
                                    recyclerView.setLayoutManager(new LinearLayoutManager(Salon.this));
                                    recyclerView.setAdapter(salonAdapter);
                                    salonAdapter.notifyDataSetChanged();

                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });


                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });


                    //mainMenus = salonMenuItems.stream().distinct().collect(Collectors.toList());
                }else {
                    ToastMaker.createToast(context,"Please try again!");
                }
*/

    }

    @Override
    public void onClick(View view) {

        if (salonMenuItems!=null){
            switch (view.getId()){
                case R.id.men:
                    loadItems(0);
                    break;
                case R.id.women:
                    loadItems(1);
                    break;

                case R.id.common:
                    loadItems(2);
                    break;
            }
        }
    }

    private void loadItems(final int j) {

      //  if (salonMenuItems!=null || salonMenuItems.size()>0) {
         //   subMenus.clear();
            for (SalonMenuItem menuItem : salonMenuItems) {
                if (mainMenus.get(j).equals(menuItem.getMainCategory())) {

                    if (!subMenus.contains(menuItem.getSubCategory())) {
                        subMenus.add(menuItem.getSubCategory());
                    }

                }
            }

            ArrayAdapter<String> subAdapter = new ArrayAdapter<String>(Salon.this, android.R.layout.simple_spinner_item, subMenus);
            subAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            subSpinner.setAdapter(subAdapter);


            subSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                    ArrayList<SalonMenuItem> item = new ArrayList<>();

                    for (SalonMenuItem menuItem : salonMenuItems) {
                        if (mainMenus.get(j).equals(menuItem.getMainCategory())) {

                            if (subSpinner.getSelectedItem().toString().equals(menuItem.getSubCategory())) {
                                item.add(menuItem);
                            }
                        }
                    }

                    SalonAdapter salonAdapter = new SalonAdapter(Salon.this, item);
                    recyclerView.setLayoutManager(new LinearLayoutManager(Salon.this));
                    recyclerView.setAdapter(salonAdapter);
                    salonAdapter.notifyDataSetChanged();

                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        //}
    }


}
