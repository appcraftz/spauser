package com.appcraftz.spamember.Activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.appcraftz.spamember.Adapters.MassageHistoryAdapter;
import com.appcraftz.spamember.Helpers.CalendarUtils;
import com.appcraftz.spamember.Helpers.DatePicker;
import com.appcraftz.spamember.Helpers.DateSet;
import com.appcraftz.spamember.Helpers.MyProgressDialog;
import com.appcraftz.spamember.Helpers.NetworkStatus;
import com.appcraftz.spamember.Items.MassageHistoryItem;
import com.appcraftz.spamember.R;
import com.appcraftz.spamember.ServerAgent.ServerAgent;

import java.util.ArrayList;

public class Collections extends AppCompatActivity {

    ServerAgent agent;
    NetworkStatus networkStatus;
    MyProgressDialog dialog;

    RecyclerView recyclerView;

    MassageHistoryAdapter adapter;
    String from = "";
    TextView selectedDate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collections);

        if (getIntent().getExtras()!=null){
            from = getIntent().getExtras().getString("from");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        setupViews(Collections.this);
        getData(from,CalendarUtils.getCurrentDate());

        selectedDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePicker datePicker = new DatePicker(selectedDate);
                datePicker.show(getFragmentManager(),"Select Date");
                datePicker.setOnDateSetClickedListener(new DateSet() {
                    @Override
                    public void onDateSet(String date) {
                        getData(from,date);
                    }
                });
            }
        });
    }

    private void setupViews(Context context) {

        agent = new ServerAgent(context);
        dialog = new MyProgressDialog(context);
        networkStatus = NetworkStatus.getInstance(context);
        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        selectedDate = (TextView)findViewById(R.id.selectedDate);
    }


    @SuppressLint("StaticFieldLeak")
    private void getData(final String type, final String date) {
        new AsyncTask<Void, Void, ArrayList<MassageHistoryItem>>() {
            @Override
            protected ArrayList<MassageHistoryItem> doInBackground(Void... voids) {
                return agent.getMassageHistory(type, date);
            }

            @Override
            protected void onPostExecute(ArrayList<MassageHistoryItem> imassageHistoryItems) {
                super.onPostExecute(imassageHistoryItems);
                if (imassageHistoryItems!=null){


                    adapter = new MassageHistoryAdapter(Collections.this,imassageHistoryItems,type);
                    recyclerView.setAdapter(adapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(Collections.this));
                    adapter.notifyDataSetChanged();

                }

            }
        }.execute(null,null,null);
    }
}
