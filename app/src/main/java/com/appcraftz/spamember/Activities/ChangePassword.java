package com.appcraftz.spamember.Activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.appcraftz.spamember.Helpers.MyProgressDialog;
import com.appcraftz.spamember.Helpers.NetworkStatus;
import com.appcraftz.spamember.Helpers.SPHelper;
import com.appcraftz.spamember.Helpers.ToastMaker;
import com.appcraftz.spamember.R;
import com.appcraftz.spamember.ServerAgent.ServerAgent;

public class ChangePassword extends AppCompatActivity {

    EditText newPin,confirmedPin,oldPin;

    Button changePassword;
    ServerAgent agent;
    MyProgressDialog dialog;
    NetworkStatus networkStatus;
    String newPass,oldPass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
    }

    @Override
    protected void onStart() {
        super.onStart();

        setupViews(ChangePassword.this);



    }

    private void setupViews(final Context context) {

        oldPin = (EditText)findViewById(R.id.oldPIN);
        newPin = (EditText)findViewById(R.id.newPIN);
        confirmedPin = (EditText)findViewById(R.id.confirmPIN);
        changePassword = (Button) findViewById(R.id.changePassword);


        agent = new ServerAgent(context);
        dialog = new MyProgressDialog(context);
        networkStatus = NetworkStatus.getInstance(context);


        changePassword.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("StaticFieldLeak")
            @Override
            public void onClick(View view) {
                if (!networkStatus.isOnline()){

                    if (oldPin.getText().toString().trim().equals("") ||
                            newPin.getText().toString().trim().equals("") ||
                            confirmedPin.getText().toString().trim().equals("")){
                        ToastMaker.emptyFields(context);

                    }else if(!newPin.getText().toString().trim().equals(confirmedPin.getText().toString().trim())){

                        ToastMaker.createToast(context,"New Password and Confirmed password doesn't match!");
                    }else {
                        new AsyncTask<Void, Void, String>() {
                            @Override
                            protected void onPreExecute() {
                                super.onPreExecute();
                                dialog.show();
                                newPass = newPin.getText().toString().trim();
                                oldPass = oldPin.getText().toString().trim();

                            }

                            @Override
                            protected String doInBackground(Void... voids) {
                                return agent.changePassword(SPHelper.getSP(context,"username"),newPass,oldPass);
                            }

                            @Override
                            protected void onPostExecute(String s) {
                                super.onPostExecute(s);
                                dialog.dismiss();
                                if (s!=null){
                                    if (s.equals("PIN Changed")){
                                        ToastMaker.createToast(context,"PIN Changed Successfully");
                                        newPin.setText("");
                                        oldPin.setText("");
                                        finish();
                                    }
                                    else if(s.equals("Invalid old PIN!")) {
                                        ToastMaker.createToast(context, "Invalid old PIN!");
                                    }else if (s.equals("Invalid Username")){
                                        ToastMaker.createToast(context, "Invalid Username");
                                    }
                                    else {
                                        ToastMaker.createToast(context, "Unable to change PIN!");
                                    }
                                }else {
                                    ToastMaker.createToast(context,"Unable to change PIN!");
                                }
                            }
                        }.execute(null,null,null);
                    }
                }else {
                    ToastMaker.noNetwork(context);
                }
            }
        });
    }
}
