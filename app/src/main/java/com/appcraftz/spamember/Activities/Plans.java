package com.appcraftz.spamember.Activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.appcraftz.spamember.Adapters.MassageAdapter;
import com.appcraftz.spamember.Adapters.OfferAdapter;
import com.appcraftz.spamember.Adapters.PlanAdapter;
import com.appcraftz.spamember.Helpers.MyProgressDialog;
import com.appcraftz.spamember.Helpers.NetworkStatus;
import com.appcraftz.spamember.Items.MassageItem;
import com.appcraftz.spamember.Items.OfferItem;
import com.appcraftz.spamember.Items.PlanItem;
import com.appcraftz.spamember.R;
import com.appcraftz.spamember.ServerAgent.ServerAgent;

import java.util.ArrayList;

public class Plans extends AppCompatActivity {

    ServerAgent agent;
    MyProgressDialog dialog;
    NetworkStatus networkStatus;
    PlanAdapter adapter;
    RecyclerView recyclerView;
    ArrayList<PlanItem> planItems;
    String type = "";
    ArrayList<MassageItem> massageItems;
    MassageAdapter massageAdapter;

    ArrayList<OfferItem> offerItems;
    OfferAdapter offerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plans);
        if (getIntent().getExtras()!=null){
            type = getIntent().getExtras().getString("type");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        setupViews(Plans.this);

        if (type.equals("plans")) {
            ((TextView)findViewById(R.id.headTag)).setText("Plans");
            getData(Plans.this);

            (findViewById(R.id.tp)).setVisibility(View.VISIBLE);
        }
        else if (type.equals("massages")) {
            ((TextView)findViewById(R.id.headTag)).setText("Massages");
            getMassages(Plans.this);
        }else if (type.equals("offers")){
            ((TextView)findViewById(R.id.headTag)).setText("Offers");
            getOffers(Plans.this);
        }else if (type.equals("salon")) {
            ((TextView)findViewById(R.id.headTag)).setText("Salon");
            getSalonMenus(Plans.this);
        }
    }

    @SuppressLint("StaticFieldLeak")
    private void getSalonMenus(final Context context) {
        new AsyncTask<Void, Void, ArrayList<MassageItem>>() {
            @Override
            protected ArrayList<MassageItem> doInBackground(Void... voids) {
                return agent.getSalonMenus();
            }

            @Override
            protected void onPostExecute(ArrayList<MassageItem> imassageItems) {
                super.onPostExecute(imassageItems);
                if (imassageItems!=null){
                    massageItems = imassageItems;
                    massageAdapter = new MassageAdapter(context,massageItems);
                    recyclerView.setAdapter(massageAdapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(context));
                    massageAdapter.notifyDataSetChanged();
                }
            }
        }.execute(null,null,null);
    }

    @SuppressLint("StaticFieldLeak")
    private void getMassages(final Context context) {
        new AsyncTask<Void, Void, ArrayList<MassageItem>>() {
            @Override
            protected ArrayList<MassageItem> doInBackground(Void... voids) {
                return agent.getMassages();
            }

            @Override
            protected void onPostExecute(ArrayList<MassageItem> imassageItems) {
                super.onPostExecute(imassageItems);
                if (imassageItems!=null){
                    massageItems = imassageItems;
                    massageAdapter = new MassageAdapter(context,massageItems);
                    recyclerView.setAdapter(massageAdapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(context));
                    massageAdapter.notifyDataSetChanged();
                }
            }
        }.execute(null,null,null);
    }

    @SuppressLint("StaticFieldLeak")
    private void getData(final Context context) {

        new AsyncTask<Void, Void, ArrayList<PlanItem>>() {
            @Override
            protected ArrayList<PlanItem> doInBackground(Void... voids) {
                return agent.getPlans();
            }

            @Override
            protected void onPostExecute(ArrayList<PlanItem> iplanItems) {
                super.onPostExecute(iplanItems);
                if (iplanItems!=null){

                    planItems = iplanItems;
                    adapter = new PlanAdapter(context,planItems);
                    recyclerView.setAdapter(adapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(context));
                    adapter.notifyDataSetChanged();
                }
            }
        }.execute(null,null,null);
    }

    @SuppressLint("StaticFieldLeak")
    private void getOffers(final Context context) {
        new AsyncTask<Void, Void, ArrayList<OfferItem>>() {
            @Override
            protected ArrayList<OfferItem> doInBackground(Void... voids) {
                return agent.getOffers();
            }

            @Override
            protected void onPostExecute(ArrayList<OfferItem> iofferItems) {
                super.onPostExecute(iofferItems);
                if (iofferItems!=null){
                    offerItems = iofferItems;

                    offerAdapter = new OfferAdapter(context,offerItems);
                    recyclerView.setAdapter(offerAdapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(context));
                    offerAdapter.notifyDataSetChanged();

                }
            }
        }.execute(null,null,null);
    }

    private void setupViews(Context context) {

        agent = new ServerAgent(context);
        dialog = new MyProgressDialog(context);
        networkStatus = NetworkStatus.getInstance(context);

        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
    }
}
