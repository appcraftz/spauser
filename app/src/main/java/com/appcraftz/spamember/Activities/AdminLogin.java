package com.appcraftz.spamember.Activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.appcraftz.spamember.Helpers.MyProgressDialog;
import com.appcraftz.spamember.Helpers.NetworkStatus;
import com.appcraftz.spamember.Helpers.SPHelper;
import com.appcraftz.spamember.Helpers.ToastMaker;
import com.appcraftz.spamember.Helpers.Utils;
import com.appcraftz.spamember.R;
import com.appcraftz.spamember.ServerAgent.ServerAgent;

public class AdminLogin extends AppCompatActivity {

    ServerAgent agent;
    MyProgressDialog dialog;

    EditText username,password;
    String uname,pwd;
    TextView loginView;
    NetworkStatus networkStatus;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_login);
    }

    @Override
    protected void onStart() {
        super.onStart();

        setupViews(AdminLogin.this);


        loginView.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("StaticFieldLeak")
            @Override
            public void onClick(View view) {

                if (!networkStatus.isOnline()){

                    if (Utils.isEmpty(username) || Utils.isEmpty(password)){
                        ToastMaker.emptyFields(AdminLogin.this);
                    }else if(password.getText().toString().length()<4){
                        ToastMaker.createToast(AdminLogin.this,"Please enter valid Password!");
                    }else {
                        new AsyncTask<Void, Void, String>() {

                            @Override
                            protected void onPreExecute() {
                                super.onPreExecute();
                                uname = username.getText().toString();
                                pwd = password.getText().toString();
                            }

                            @Override
                            protected String doInBackground(Void... voids) {
                                return agent.loginAdmin(uname,pwd);
                            }

                            @Override
                            protected void onPostExecute(String s) {
                                super.onPostExecute(s);
                                if (s!=null){

                                    if (s.equals("ENTERED")){
                                        ToastMaker.createToast(AdminLogin.this,"Login Successful");
                                        SPHelper.setSP(AdminLogin.this,"username",uname);
                                        SPHelper.setSP(AdminLogin.this,"isLoggedIn","true");
                                        startActivity(new Intent(AdminLogin.this, MainActivity.class));
                                        finish();
                                    }else {
                                        ToastMaker.createToast(AdminLogin.this,"Please enter valid credentials!");
                                    }
                                }else {
                                    ToastMaker.createToast(AdminLogin.this,"Unable to login please try again!");
                                }

                            }
                        }.execute(null,null,null);
                    }
                }else {
                    ToastMaker.noNetwork(AdminLogin.this);
                }

            }
        });
    }

    private void setupViews(Context context) {

        username = (EditText)findViewById(R.id.number);
        password = (EditText)findViewById(R.id.password);
        loginView = (TextView)findViewById(R.id.login);

        agent = new ServerAgent(context);
        dialog = new MyProgressDialog(context);
        networkStatus = NetworkStatus.getInstance(context);


    }

}
