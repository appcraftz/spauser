package com.appcraftz.spamember.Activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.appcraftz.spamember.Helpers.MyProgressDialog;
import com.appcraftz.spamember.Helpers.NetworkStatus;
import com.appcraftz.spamember.Helpers.ToastMaker;
import com.appcraftz.spamember.Helpers.Utils;
import com.appcraftz.spamember.R;
import com.appcraftz.spamember.ServerAgent.ServerAgent;

public class AddOffer extends AppCompatActivity {

    ServerAgent agent ;
    NetworkStatus networkStatus;
    MyProgressDialog dialog;
    EditText discount;
    Button addDiscount;
    String disc,discType;
    Spinner discountType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_offer);
    }

    @Override
    protected void onStart() {
        super.onStart();

        setupViews(AddOffer.this);

    }

    private void setupViews(final Context context) {
        agent = new ServerAgent(context);
        networkStatus = NetworkStatus.getInstance(context);
        dialog = new MyProgressDialog(context);

        discount = (EditText)findViewById(R.id.discountAmount);
        addDiscount = (Button)findViewById(R.id.offer);
        discountType = (Spinner) findViewById(R.id.discountType);

        addDiscount.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("StaticFieldLeak")
            @Override
            public void onClick(View view) {

                    if (Utils.isEmpty(discount)){
                        ToastMaker.emptyFields(context);
                    }else {
                        new AsyncTask<Void, Void, String>() {
                            @Override
                            protected void onPreExecute() {
                                super.onPreExecute();
                                disc = discount.getText().toString();
                                if (discountType.getSelectedItemPosition()==0)
                                    discType = "1";
                                else
                                    discType = "0";
                                dialog.show();
                            }

                            @Override
                            protected String doInBackground(Void... voids) {
                                return agent.addOffer(disc,discType);
                            }

                            @Override
                            protected void onPostExecute(String s) {
                                super.onPostExecute(s);
                                dialog.dismiss();
                                if (s!=null){
                                    if (s.equals("Discount Added Successfully")){
                                        ToastMaker.createToast(context,"Discount Added Successfully");
                                        discount.setText("");
                                    }else {
                                        ToastMaker.createToast(context,"Unable to add Discount!");
                                    }
                                }
                            }
                        }.execute(null,null,null);
                    }

            }
        });


    }
}
