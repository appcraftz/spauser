package com.appcraftz.spamember.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.appcraftz.spamember.R;

public class ChooseMainCategory extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_main_category);
    }

    @Override
    protected void onStart() {
        super.onStart();


        (findViewById(R.id.men)).setOnClickListener(this);
        (findViewById(R.id.women)).setOnClickListener(this);
        (findViewById(R.id.common)).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {


        Intent intent = null;
        Bundle bundle ;

        switch (view.getId()){

            case R.id.men:
                intent = new Intent(ChooseMainCategory.this,Salon.class);
                bundle = new Bundle();
                bundle.putString("type","MEN");
                intent.putExtras(bundle);
                break;

            case R.id.common:
                intent = new Intent(ChooseMainCategory.this,Salon.class);
                bundle = new Bundle();
                bundle.putString("type","COMMON");
                intent.putExtras(bundle);
                break;

            case R.id.women:
                intent = new Intent(ChooseMainCategory.this,Salon.class);
                bundle = new Bundle();
                bundle.putString("type","WOMEN");
                intent.putExtras(bundle);
                break;
        }

        if (intent!=null)
            startActivity(intent);
    }
}
