package com.appcraftz.spamember.Activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appcraftz.spamember.Helpers.SPHelper;
import com.appcraftz.spamember.Login;
import com.appcraftz.spamember.R;

public class MainDashboard extends AppCompatActivity implements View.OnClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_dashboard_alternative);
    }

    @Override
    protected void onStart() {
        super.onStart();


        if (SPHelper.getSP(MainDashboard.this,"isLoggedIn").equals("true")){
            ((TextView)findViewById(R.id.loginText)).setText("Logout");
        }else {
            ((TextView)findViewById(R.id.loginText)).setText("Login");
        }


        findViewById(R.id.massagesCard).setOnClickListener(this);
        findViewById(R.id.plansCard).setOnClickListener(this);
        findViewById(R.id.aboutCard).setOnClickListener(this);
        findViewById(R.id.offersCard).setOnClickListener(this);
        findViewById(R.id.shareCard).setOnClickListener(this);
        findViewById(R.id.benefitsCard).setOnClickListener(this);
        findViewById(R.id.galleryCard).setOnClickListener(this);
        /*findViewById(R.id.salonCard).setOnClickListener(this);
        *  findViewById(R.id.loginCard).setOnClickListener(this);
        *  findViewById(R.id.membershipCard).setOnClickListener(this);
        *  */




    }

    @Override
    public void onClick(View view) {
        Intent intent = null;
        Bundle bundle = new Bundle();

        switch (view.getId()){


            case R.id.benefitsCard:
                intent = new Intent(MainDashboard.this, Benefits.class);

                break;

            case R.id.massagesCard:
                intent = new Intent(MainDashboard.this, ChooseActivity.class);
                break;


            case R.id.salonCard:
                intent = new Intent(MainDashboard.this, ChooseActivity.class);

                break;

            case R.id.galleryCard:
                intent = new Intent(MainDashboard.this, Gallery.class);
                break;

            case R.id.plansCard:
                intent = new Intent(MainDashboard.this, Plans.class);
                bundle.putString("type","plans");
                intent.putExtras(bundle);
                break;

            case R.id.offersCard:
                intent = new Intent(MainDashboard.this, Plans.class);
                bundle.putString("type","offers");
                intent.putExtras(bundle);
                break;

            case R.id.aboutCard:
                intent = new Intent(MainDashboard.this, AboutUs.class);
                break;

            case R.id.shareCard:


                final String shareMsg = "Download Caelum Spa mobile app to check your membership status and stay updated with our offers\n https://bit.ly/2w6HZcz";

                View alertLAyout = LayoutInflater.from(MainDashboard.this).inflate(R.layout.sharelayout,null);
                AlertDialog.Builder alert = new AlertDialog.Builder(MainDashboard.this);
                alert.setTitle("Share");
                alert.setView(alertLAyout);

                LinearLayout byMsg,byEmail,byWp;

                byMsg = (LinearLayout)alertLAyout.findViewById(R.id.byMsg);
                byEmail = (LinearLayout)alertLAyout.findViewById(R.id.byEmail);
                byWp = (LinearLayout)alertLAyout.findViewById(R.id.byWp);

                byMsg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Uri uri = Uri.parse("smsto:");
                        Intent it = new Intent(Intent.ACTION_SENDTO, uri);
                        it.putExtra("sms_body", shareMsg);
                        startActivity(it);
                    }
                });

                byEmail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Uri uri = Uri.parse("mailto:")
                                .buildUpon()
                                .appendQueryParameter("subject", "Caelum Spa")
                                .appendQueryParameter("body", shareMsg)
                                .build();

                        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, uri);
                        startActivity(Intent.createChooser(emailIntent, "Choose"));
                    }
                });

                byWp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            Intent sendIntent = new Intent("android.intent.action.MAIN");
                            //sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
                            sendIntent.setAction(Intent.ACTION_SEND);
                            sendIntent.setType("text/plain");
                            sendIntent.putExtra(Intent.EXTRA_TEXT, shareMsg);
                            //sendIntent.putExtra("jid", smsNumber + "@s.whatsapp.net"); //phone number without "+" prefix
                            sendIntent.setPackage("com.whatsapp");
                            startActivity(sendIntent);
                        } catch(Exception e) {
                            Toast.makeText(MainDashboard.this, "Error/n" + e.toString(), Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }
                });

                alert.show();





                break;



                case R.id.membershipCard:

                    if (SPHelper.getSP(MainDashboard.this,"isLoggedIn").equals("true")){
                        startActivity(new Intent(MainDashboard.this,MemberDashboard.class));
                    }else {
                        startActivity(new Intent(MainDashboard.this,Login.class));
                    }

                    break;

            case R.id.loginCard:

                if (SPHelper.getSP(MainDashboard.this,"isLoggedIn").equals("true")){
                    new AlertDialog.Builder(MainDashboard.this)
                            .setMessage("Are you sure to Logout?")
                            .setPositiveButton("Logout", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                    SPHelper.setSP(MainDashboard.this,"isLoggedIn","false");
                                    ((TextView)findViewById(R.id.loginText)).setText("Login");
                                    //startActivity(new Intent(MainDashboard.this, Login.class));
                                    //finish();
                                }
                            })
                            .setNegativeButton("Cancel",null)
                            .show();

                }else {
                    startActivity(new Intent(MainDashboard.this, Login.class));
                }


                break;
        }
        if (intent!=null)
        startActivity(intent);
    }
}
