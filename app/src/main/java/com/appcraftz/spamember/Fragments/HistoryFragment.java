package com.appcraftz.spamember.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.appcraftz.spamember.Adapters.MassageHistoryAdapter;
import com.appcraftz.spamember.Helpers.CalendarUtils;
import com.appcraftz.spamember.Helpers.MyProgressDialog;
import com.appcraftz.spamember.Helpers.NetworkStatus;
import com.appcraftz.spamember.Items.MassageHistoryItem;
import com.appcraftz.spamember.R;
import com.appcraftz.spamember.ServerAgent.ServerAgent;

import java.util.ArrayList;


public class HistoryFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    String from;
    RecyclerView recyclerView;
    ServerAgent agent;
    NetworkStatus networkStatus;
    MyProgressDialog dialog;
    MassageHistoryAdapter adapter;

    public HistoryFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static HistoryFragment newInstance(String param1, String param2) {
        HistoryFragment fragment = new HistoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);


            from = getArguments().getString("from");
            Log.e("From is", from);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_history, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        setupViews(getActivity());

        /*if (getArguments().getString("type")!=null)
        getData(getArguments().getString("type"));
*/
        getData(from);
    }

    @SuppressLint("StaticFieldLeak")
    private void getData(final String type) {
        new AsyncTask<Void, Void, ArrayList<MassageHistoryItem>>() {
            @Override
            protected ArrayList<MassageHistoryItem> doInBackground(Void... voids) {
                return agent.getMassageHistory(type, CalendarUtils.getCurrentDate());
            }

            @Override
            protected void onPostExecute(ArrayList<MassageHistoryItem> imassageHistoryItems) {
                super.onPostExecute(imassageHistoryItems);
                if (imassageHistoryItems!=null){

                    adapter = new MassageHistoryAdapter(getActivity(),imassageHistoryItems,type);
                    recyclerView.setAdapter(adapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                    adapter.notifyDataSetChanged();

                }

            }
        }.execute(null,null,null);
    }

    private void setupViews(Context context) {

        agent = new ServerAgent(context);
        dialog = new MyProgressDialog(context);
        networkStatus = NetworkStatus.getInstance(context);
        recyclerView = (RecyclerView)getView().findViewById(R.id.recyclerView);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        /*if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }*/
    }


}
