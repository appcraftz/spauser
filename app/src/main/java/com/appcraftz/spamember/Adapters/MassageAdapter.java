package com.appcraftz.spamember.Adapters;
import android.content.Context;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appcraftz.spamember.Items.MassageItem;
import com.appcraftz.spamember.R;

import java.util.ArrayList;


/**
 * Created by appcraftz on 14/3/18.
 */

public class MassageAdapter extends RecyclerView.Adapter<MassageAdapter.viewHolder> {
    Context context;
    ArrayList<MassageItem> massageItems;

    public MassageAdapter(Context context,ArrayList<MassageItem> massageItems){
        this.context = context;
        this.massageItems = massageItems;
    }

    @Override
    public MassageAdapter.viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new viewHolder(LayoutInflater.from(context).inflate(R.layout.planitem,null));
    }

    @Override
    public void onBindViewHolder(MassageAdapter.viewHolder holder, int position) {

        holder.name.setText(massageItems.get(position).getName());
        holder.amount.setText(massageItems.get(position).getAmount());
        holder.coupons.setText(massageItems.get(position).getCoupon());
       // holder.imageView.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        if (massageItems!=null)
            return massageItems.size();
        else
            return 0;
    }

    public class viewHolder extends RecyclerView.ViewHolder{

        TextView name,amount,coupons;
       // ImageView imageView;
        public viewHolder(View itemView) {
            super(itemView);

            name = (TextView)itemView.findViewById(R.id.planName);
            amount = (TextView)itemView.findViewById(R.id.planAmount);
            coupons = (TextView)itemView.findViewById(R.id.planCoupons);
         //   imageView = (ImageView) itemView.findViewById(R.id.badge);
        }
    }
}
