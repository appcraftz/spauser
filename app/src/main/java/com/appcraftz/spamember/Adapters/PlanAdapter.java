package com.appcraftz.spamember.Adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.appcraftz.spamember.Items.PlanItem;
import com.appcraftz.spamember.R;

import java.util.ArrayList;

/**
 * Created by appcraftz on 14/3/18.
 */

public class PlanAdapter extends RecyclerView.Adapter<PlanAdapter.viewHolder> {
    Context context;
    ArrayList<PlanItem> planItems;

    public PlanAdapter(Context context,ArrayList<PlanItem> planItems){
        this.context = context;
        this.planItems = planItems;
    }

    @Override
    public PlanAdapter.viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new viewHolder(LayoutInflater.from(context).inflate(R.layout.massageitem,null));
    }

    @Override
    public void onBindViewHolder(PlanAdapter.viewHolder holder, int position) {

        holder.name.setText(planItems.get(position).getName());
        holder.amount.setText(planItems.get(position).getAmount());
        holder.coupons.setText(planItems.get(position).getCoupons());

       /* if (position != 0 && position % 2 == 0)
            holder.back.setBackgroundColor(context.getResources().getColor(R.color.minThirdItem));
        else
            holder.back.setBackgroundColor(context.getResources().getColor(R.color.lightItem));*/
    }

    @Override
    public int getItemCount() {
        if (planItems!=null)
            return planItems.size();
        else
            return 0;
    }

    public class viewHolder extends RecyclerView.ViewHolder{

        TextView name,amount,coupons;
        //FrameLayout back;
        public viewHolder(View itemView) {
            super(itemView);

            name = (TextView)itemView.findViewById(R.id.planName);
            amount = (TextView)itemView.findViewById(R.id.planAmount);
            coupons = (TextView)itemView.findViewById(R.id.planCoupons);
          //  back = (FrameLayout) itemView.findViewById(R.id.cardView);
        }
    }
}
