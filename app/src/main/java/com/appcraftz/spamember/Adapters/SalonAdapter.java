package com.appcraftz.spamember.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appcraftz.spamember.Items.SalonMenuItem;
import com.appcraftz.spamember.R;

import java.util.ArrayList;

public class SalonAdapter extends RecyclerView.Adapter<SalonAdapter.viewHolder> {

    ArrayList<SalonMenuItem> salonMenuItems;
    Context context;

    public SalonAdapter(Context context,ArrayList<SalonMenuItem> salonMenuItems)
    {
        this.context = context;
        this.salonMenuItems = salonMenuItems;
    }

    @Override
    public SalonAdapter.viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SalonAdapter.viewHolder(LayoutInflater.from(context).inflate(R.layout.salonitem,null));
    }

    @Override
    public void onBindViewHolder(SalonAdapter.viewHolder holder, int position) {

        holder.name.setText(salonMenuItems.get(position).getItem());
        holder.amount.setText(salonMenuItems.get(position).getCost());
        //holder.coupons.setText(salonMenuItems.get(position).getCoupon());
    }

    @Override
    public int getItemCount() {
        if (salonMenuItems!=null)
            return salonMenuItems.size();
        else
            return 0;
    }


    public class viewHolder extends RecyclerView.ViewHolder{


        TextView name,amount,coupons;
        // ImageView imageView;
        public viewHolder(View itemView) {
            super(itemView);

            name = (TextView)itemView.findViewById(R.id.planName);
            amount = (TextView)itemView.findViewById(R.id.planAmount);
            //coupons = (TextView)itemView.findViewById(R.id.planCoupons);
            //   imageView = (ImageView) itemView.findViewById(R.id.badge);
        }
    }
}
