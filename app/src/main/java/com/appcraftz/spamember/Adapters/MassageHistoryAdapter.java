package com.appcraftz.spamember.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appcraftz.spamember.Items.MassageHistoryItem;
import com.appcraftz.spamember.R;


import java.util.ArrayList;

/**
 * Created by appcraftz on 21/3/18.
 */

public class MassageHistoryAdapter extends RecyclerView.Adapter<MassageHistoryAdapter.viewHolder> {

    Context context;
    ArrayList<MassageHistoryItem> massageHistoryItems;
    String type;

    public MassageHistoryAdapter(Context context,ArrayList<MassageHistoryItem> massageHistoryItems,String type){
        this.context =context;
        this.massageHistoryItems = massageHistoryItems;
        this.type = type;
    }


    @Override
    public MassageHistoryAdapter.viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new viewHolder(LayoutInflater.from(context).inflate(R.layout.massagehistoryitem,null));
    }

    @Override
    public void onBindViewHolder(MassageHistoryAdapter.viewHolder holder, int position) {

        holder.hName.setText(massageHistoryItems.get(position).getName());
        holder.hMassage.setText(massageHistoryItems.get(position).getMassageName());
        holder.hDate.setText(massageHistoryItems.get(position).getDate());
        holder.hTName.setText(massageHistoryItems.get(position).getTherapistName());


        if (type.equals("member")){
            holder.hAmount.setText("Coupons : "+massageHistoryItems.get(position).getAmount());
            holder.hBCoup.setVisibility(View.VISIBLE);
            holder.hBCoup.setText("Balance Coupons : "+massageHistoryItems.get(position).getBalanceCoupons());
        }
        else if (type.equals("daily")) {
            holder.hAmount.setText("Amount : " + massageHistoryItems.get(position).getAmount());
        }
    }

    @Override
    public int getItemCount() {
        if (massageHistoryItems!=null)
            return massageHistoryItems.size();
        else
            return 0;
    }

    public class viewHolder extends RecyclerView.ViewHolder{

        TextView hName,hMassage,hDate,hAmount,hTName,hBCoup;
        public viewHolder(View itemView) {
            super(itemView);

            hName = (TextView)itemView.findViewById(R.id.hCustomerName);
            hMassage = (TextView)itemView.findViewById(R.id.hMassageName);
            hDate = (TextView)itemView.findViewById(R.id.hHistoryDate);
            hAmount = (TextView)itemView.findViewById(R.id.hAmount);
            hTName = (TextView)itemView.findViewById(R.id.hTName);
            hBCoup = (TextView)itemView.findViewById(R.id.hBCoup);
        }
    }
}
