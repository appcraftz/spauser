package com.appcraftz.spamember.Adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.appcraftz.spamember.Interfaces.ApprovalClicked;
import com.appcraftz.spamember.Items.MemberItem;
import com.appcraftz.spamember.R;

import java.util.ArrayList;

/**
 * Created by appcraftz on 29/3/18.
 */

public class ApprovalAdapter extends RecyclerView.Adapter<ApprovalAdapter.viewHolder> {

    Context context;
    ArrayList<MemberItem> memberItems;
    ApprovalClicked approvalClicked;
    String type;
    public ApprovalAdapter (Context context,ArrayList<MemberItem> memberItems,String type){
        this.context = context;
        this.memberItems = memberItems;
        this.type = type;
    }

    @Override
    public ApprovalAdapter.viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new viewHolder(LayoutInflater.from(context).inflate(R.layout.memberitem,null));
    }

    @Override
    public void onBindViewHolder(ApprovalAdapter.viewHolder holder, final int position) {
        holder.name.setText(memberItems.get(position).getName());
        holder.date.setText(memberItems.get(position).getJoiningDate());
        holder.plan.setText(memberItems.get(position).getSelectedPlanName());

        holder.approve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (approvalClicked!=null){
                    approvalClicked.onApprovalClicked(memberItems.get(position),true);
                }
            }
        });
        holder.reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (approvalClicked!=null){
                    approvalClicked.onApprovalClicked(memberItems.get(position),false);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        if (memberItems!=null)
            return memberItems.size();
        else
            return 0;
    }

    public class viewHolder extends RecyclerView.ViewHolder{
        TextView name,date,plan;
        CardView memberLayout;
        Button approve,reject;
        public viewHolder(View itemView) {
            super(itemView);

            name = (TextView)itemView.findViewById(R.id.memName);
            date = (TextView)itemView.findViewById(R.id.memDate);
            plan = (TextView)itemView.findViewById(R.id.memPlan);
            memberLayout = (CardView) itemView.findViewById(R.id.memberLayout);
            approve = (Button) itemView.findViewById(R.id.approve);
            reject = (Button)itemView.findViewById(R.id.reject);
        }
    }
    public void setOnApprovalClickedListener(ApprovalClicked approvalClicked){
        this.approvalClicked = approvalClicked;
    }
}
