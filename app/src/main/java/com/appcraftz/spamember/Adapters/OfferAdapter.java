package com.appcraftz.spamember.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.appcraftz.spamember.Interfaces.DeleteOffer;
import com.appcraftz.spamember.Items.OfferItem;
import com.appcraftz.spamember.R;
import com.appcraftz.spamember.Items.OfferItem;

import java.util.ArrayList;

public class OfferAdapter extends RecyclerView.Adapter<OfferAdapter.viewHolder> {

    ArrayList<OfferItem> offerItems;
    Context context;

    DeleteOffer deleteOffer;

    public OfferAdapter(Context context, ArrayList<OfferItem> offerItems){
        this.context = context;
        this.offerItems = offerItems;
    }

    @Override
    public OfferAdapter.viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new viewHolder(LayoutInflater.from(context).inflate(R.layout.offeritem2,null));
    }

    @Override
    public void onBindViewHolder(OfferAdapter.viewHolder holder, final int position) {

        holder.offer.setText(offerItems.get(position).getOffer());

    }

    @Override
    public int getItemCount() {
        if (offerItems!=null){
            return offerItems.size();
        }else {
            return 0;
        }
    }

    public class viewHolder extends RecyclerView.ViewHolder{

        TextView offer;

        public viewHolder(View itemView) {
            super(itemView);

            offer = (TextView)itemView.findViewById(R.id.offername);

        }
    }

    public void setOnOfferDeleteClicked(DeleteOffer deleteOffer){
        this.deleteOffer =deleteOffer;
    }
}
