package com.appcraftz.spamember.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.appcraftz.spamember.Fragments.HistoryFragment;

/**
 * Created by appcraftz on 13/9/17.
 */

public class SimpleFragmentPagerAdapter extends FragmentPagerAdapter {

    private Context context;


    public SimpleFragmentPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        Bundle bundle = new Bundle();
        if (position == 0){
            bundle.putString("from","member");
            HistoryFragment fragment = new HistoryFragment();
            fragment.setArguments(bundle);
            return fragment;
        }
        else if (position == 1) {
            bundle.putString("from", "daily");
            HistoryFragment fragment = new HistoryFragment();
            fragment.setArguments(bundle);
            return fragment;
        }
        else {
            bundle.putString("from", "member");
            HistoryFragment fragment = new HistoryFragment();
            fragment.setArguments(bundle);
            return fragment;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "Members";

            case 1:
                return "Daily Customers";

            default:
                return null;
        }
    }
}
