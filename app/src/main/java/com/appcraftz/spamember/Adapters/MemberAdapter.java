package com.appcraftz.spamember.Adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appcraftz.spamember.Interfaces.MemberItemClicked;
import com.appcraftz.spamember.Items.MemberItem;
import com.appcraftz.spamember.R;

import java.util.ArrayList;

/**
 * Created by appcraftz on 20/3/18.
 */

public class MemberAdapter extends RecyclerView.Adapter<MemberAdapter.viewHolder> {

    Context context;
    ArrayList<MemberItem> memberItems;
    MemberItemClicked memberItemClicked;

    public MemberAdapter(Context context,ArrayList<MemberItem> memberItems){
        this.context = context;
        this.memberItems = memberItems;
    }

    @Override
    public MemberAdapter.viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new viewHolder(LayoutInflater.from(context).inflate(R.layout.memberitem2,null));
    }

    @Override
    public void onBindViewHolder(MemberAdapter.viewHolder holder, final int position) {

        holder.name.setText(memberItems.get(position).getName());
        holder.date.setText(memberItems.get(position).getJoiningDate());
        holder.plan.setText(memberItems.get(position).getSelectedPlanName());


        holder.memberLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (memberItemClicked!=null){
                    memberItemClicked.onMemberItemClicked(memberItems.get(position));
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        if (memberItems!=null)
            return memberItems.size();
        else
            return 0;
    }

    public class viewHolder extends RecyclerView.ViewHolder{

        TextView name,date,plan;
        CardView memberLayout;
        public viewHolder(View itemView) {
            super(itemView);
            name = (TextView)itemView.findViewById(R.id.memName);
            date = (TextView)itemView.findViewById(R.id.memDate);
            plan = (TextView)itemView.findViewById(R.id.memPlan);
            memberLayout = (CardView) itemView.findViewById(R.id.memberLayout);
        }
    }

    public void setOnMemberItemClickedListener(MemberItemClicked memberItemClicked){
        this.memberItemClicked = memberItemClicked;
    }
}
