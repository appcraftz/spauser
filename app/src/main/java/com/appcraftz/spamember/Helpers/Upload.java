package com.appcraftz.spamember.Helpers;

import android.content.Context;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

//import android.util.Log;


public class Upload {

    public static String UPLOAD_URL;//= "http://levashubhmangal.com/web_services/upload_photo_exif.php?"+"&prof_id=" + SPHelper.getSP(con,"prof_id")+"/";
    //public static final String UPLOAD_URL_FORM_PHOTO = "http://levashubhmangal.com/web_services/form_photo.php?";


    private int serverResponseCode;

    public String uploadEventPhoto(String fname, Context con) {

        //UPLOAD_URL= Url.uploadFile;//+"&prof_id=" + SPHelper.getSP(con,"prof_id");
        UPLOAD_URL= "http://217.61.17.155/~appcrf/development/SalesTrackNew/upload_photo_exif.php?";
        String fileName = fname;//file;
       // Log.e("Photo path",fileName);
        String temp_start = fname.substring(0,fileName.lastIndexOf("/")+1);
        String temp_end = fname.substring(fileName.lastIndexOf("/")+1);

      //  temp_end = eventItem.getUid() +"_1_" + temp_end;

        //fileName = temp_start + temp_end;
        //Log.e("TEMP",fileName);
        ////Log.e("TEMP end",temp_end);



        //Log.e("FILE NAME",fileName);
                HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;

        File sourceFile=null;
        try {
           sourceFile = new File(fname);
        }catch (Exception e){e.printStackTrace();}
        if (!sourceFile.isFile()) {

            return null;
        }

        try {
            FileInputStream fileInputStream = new FileInputStream(sourceFile);
            URL url;
           // if(type==1)
            url = new URL(UPLOAD_URL);
           // else
             //   url = new URL(UPLOAD_URL_FORM_PHOTO);
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");

            conn.setRequestProperty("ENCTYPE", "multipart/form-data");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            conn.setRequestProperty("myFile", fileName);
            dos = new DataOutputStream(conn.getOutputStream());

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"myFile\";filename=\"" +fileName + "\"" + lineEnd);
            dos.writeBytes(lineEnd);


            bytesAvailable = fileInputStream.available();
           // Log.e("Huzza", "Initial .available : " + bytesAvailable);

            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];

            bytesRead = fileInputStream.read(buffer, 0, bufferSize);

            while (bytesRead > 0) {
                dos.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }

            dos.writeBytes(lineEnd);
            dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);


          //  Log.e("RESPONSE MESSAGE",conn.getResponseMessage());
            serverResponseCode = conn.getResponseCode();

            fileInputStream.close();
            dos.flush();
            dos.close();


        } catch (MalformedURLException ex) {
            //Log.e("ERROR 1","ERROR 1");
            ex.printStackTrace();
        } catch (Exception e) {
          //  Log.e("ERROR 2","ERROR 2");
            e.printStackTrace();
        }

        if (serverResponseCode == 200) {
           // Log.e("RESPONSE OK","RESPONSE OK");

            StringBuilder sb = new StringBuilder();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(conn
                        .getInputStream()));
                String line;
                while ((line = rd.readLine()) != null) {
                    sb.append(line);
                   // Log.e("line",line);
                }
                rd.close();

               // Log.e("Response",sb.toString());

/*
                ArrayList<NameValuePair> fName = new ArrayList<>();
                fName.add(new BasicNameValuePair("file_name",fileName.substring(fileName.lastIndexOf("/")+1)));
                fName.add(new BasicNameValuePair("prof_id",SPHelper.getSP(con,"prof_id")));
                if(type==1)
                SPHelper.setSP(con,"photoFileName","upload/"+fileName.substring(fileName.lastIndexOf("/")+1));

                NetworkUtils networkUtils = new NetworkUtils();

                if(type==1)
                networkUtils.getNormalResponce(Api.updatePhotoName,fName);
                else
                    networkUtils.getNormalResponce(Api.updateFormName,fName);

*/

               // Log.e("file name" ,fileName.substring(fileName.lastIndexOf("/")+1));

//eventItem.setPhotoUrl(fileName.substring(fileName.lastIndexOf("/")+1));
            return fileName.substring(fileName.lastIndexOf("/")+1);

            } catch (IOException ioex) {
            //    Log.e("ERROR 3","ERROR 3");
                ioex.printStackTrace();
            }
            return sb.toString();
        }else {
         //   Log.e("FAILED","UPLOAD FAILED");
            return "FAILED_TO_UPLOAD";
        }
    }
}
