package com.appcraftz.spamember.Helpers;

import android.text.TextUtils;
import android.widget.EditText;

/**
 * Created by appcraftz on 24/2/18.
 */

public class Utils {
    public static boolean isEmpty(EditText editText){
        if (editText!=null)
            return TextUtils.isEmpty(editText.getText().toString());
        else {
            LogMaker.writeLog("EditText is Null","Yup");
            return false;
        }
    }
}
