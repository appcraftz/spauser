package com.appcraftz.spamember.Helpers;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

/**
 * Created by appcraftz on 19/1/18.
 */

public class ToastMaker {

    public static void createToast(Context context,String message){
        Toast toast = Toast.makeText(context,message,Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER,0,0);
        toast.show();
    }

    public static void emptyFields(Context context){
        Toast toast = Toast.makeText(context,"Empty fields not allowed!",Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER,0,0);
        toast.show();
    }

    public static void noNetwork(Context context){
        Toast toast = Toast.makeText(context,"No Internet Connection please try again!",Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER,0,0);
        toast.show();
    }

}
