package com.appcraftz.spamember.Helpers;

/**
 * Created by appcraftz on 9/10/17.
 */

public interface DateSet {
    public void onDateSet(String date);
}
