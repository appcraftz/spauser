package com.appcraftz.spamember.Helpers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by appcraftz on 12/3/18.
 */

public class SharefPref {

    public static final String PREFERENCE = "rahul";

    public static void setValue(Context context, String key, String value){
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getValue(Context context, String key){
        SharedPreferences prefs = context.getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
        String value = prefs.getString(key, null);
        if (value != null) {
            return  value;
        }
        return "empty";
    }
}
