package com.appcraftz.spamember.Helpers;

import android.text.Html;

import java.util.ArrayList;
import java.util.Calendar;

//import android.util.Log;

/**
 * Created by appcraftz on 24/07/17.
 */

public class CalendarUtils {

    static Calendar calendar = Calendar.getInstance();

    public static String getMonthName(){




        int month = calendar.get(Calendar.MONTH);
        ArrayList<String> months = new ArrayList<>();
        months.add("January");
        months.add("February");
        months.add("March");
        months.add("April");
        months.add("May");
        months.add("June");
        months.add("July");
        months.add("August");
        months.add("September");
        months.add("October");
        months.add("November");
        months.add("December");


        return months.get(month);
    }
    public static int getMonthNumberByName(String name)
    {

        ArrayList<String> months = new ArrayList<>();
        months.add("January");
        months.add("February");
        months.add("March");
        months.add("April");
        months.add("May");
        months.add("June");
        months.add("July");
        months.add("August");
        months.add("September");
        months.add("October");
        months.add("November");
        months.add("December");


        return months.indexOf(name);
    }

    public static int getMaxDaysOfMonth(int month)
    {
        calendar.set(Calendar.MONTH,month);
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }
    public static String getMonthName(int month){





        ArrayList<String> months = new ArrayList<>();
        months.add("January");
        months.add("February");
        months.add("March");
        months.add("April");
        months.add("May");
        months.add("June");
        months.add("July");
        months.add("August");
        months.add("September");
        months.add("October");
        months.add("November");
        months.add("December");


        return months.get(month);
    }

    public static String getFullDate()
    {
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        String daywithpostfix;
        if(day==1||day==31)
        {
            daywithpostfix = Html.fromHtml(String.valueOf(day) + "<sup>st</sup>").toString();

        }else if(day==2)
        {
            daywithpostfix = Html.fromHtml(String.valueOf(day) + "<sup>nd</sup>").toString();
        }else if(day==3)
        {
            daywithpostfix = Html.fromHtml(String.valueOf(day) + "<sup>rd</sup>").toString();
        }else
        {
            daywithpostfix = Html.fromHtml(String.valueOf(day) + "<sup>th</sup>").toString();
        }
        String date = daywithpostfix + " " + getMonthName() + " " + getCurrentYear();
        return date;
    }

    public static String getCurrentMonth()
    {
        return String.valueOf(calendar.get(Calendar.MONTH));
    }
    public static String getCurrentDate()
    {
        final Calendar calendar = Calendar.getInstance();
        final String day = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
        final String month = String.valueOf(calendar.get(Calendar.MONTH )+1);
        final String year = String.valueOf(calendar.get(Calendar.YEAR));

        return day + "-" +  month+ "-" + year;
    }

    public static String getCurrentWeek()
    {
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        int week = calendar.get(Calendar.WEEK_OF_YEAR);

        return String.valueOf(week);
    }

    public static String getCurrentYear()
    {
        return String.valueOf(calendar.get(Calendar.YEAR));
    }

    public static String getTimeWithSecondsWithNoSep()
    {
        return String.valueOf(calendar.get(Calendar.HOUR_OF_DAY)) + String.valueOf(calendar.get(Calendar.MINUTE)) + String.valueOf(calendar.get(Calendar.SECOND));
    }

    public static String getCurrentTime(){

        int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
        int minute =calendar.get(Calendar.MINUTE);
        String time = (hourOfDay>9? String.valueOf(hourOfDay):("0"+ String.valueOf(hourOfDay)))+":"+ (minute>9? String.valueOf(minute):("0"+ String.valueOf(minute)));
      //  Log.e("TIME STAMP",time);

        return time;
    }
}
