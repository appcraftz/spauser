package com.appcraftz.spamember.Helpers;

import android.content.Context;
import android.text.format.DateFormat;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by appcraftz on 18/11/17.
 */

public class StringToEpoch {

    public static String converttoEpoch(Context context, String date, String time){

        String[] dateparts = date.split("-");
        String[] timeparts = time.split(":");

        int day = Integer.parseInt(dateparts[0]);
        int month = Integer.parseInt(dateparts[1]);
        int year = Integer.parseInt(dateparts[2]);

        int hour = Integer.parseInt(timeparts[0]);
        int minute = Integer.parseInt(timeparts[1]);


        Calendar myAlarmDate = Calendar.getInstance();
        myAlarmDate.setTimeInMillis(System.currentTimeMillis());

        if(DateFormat.is24HourFormat(context)){
            myAlarmDate.set(Calendar.HOUR_OF_DAY, hour);
        }
        else {
            myAlarmDate.set(Calendar.HOUR, hour);
        }

        myAlarmDate.set(Calendar.MINUTE, minute);
        myAlarmDate.set(Calendar.SECOND, 0);
        myAlarmDate.set(Calendar.MILLISECOND, 0);
        myAlarmDate.set(Calendar.DAY_OF_MONTH,day);
        myAlarmDate.set(Calendar.MONTH,month-1);
        myAlarmDate.set(Calendar.YEAR,year);

        return String.valueOf(myAlarmDate.getTimeInMillis());
    }
/*
    public static String convertTo24Hour(String Time) {
        DateFormat f1 = new SimpleDateFormat("hh:mm a"); //11:00 pm
        Date d = null;
        try {
            d = f1.parse(Time);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        DateFormat f2 = new SimpleDateFormat("HH:mm");
        String x = f2.format(d); // "23:00"

        return x;
    }*/


public static boolean CompareCurrentWithChoosed(Context context, String date, String time){

    String cuDate = CalendarUtils.getCurrentDate();
    String cuTime = CalendarUtils.getCurrentTime();

    String choosedDate = date;
    String choosedTime = time;

    Log.e("Current Date and Time", cuDate +" "+ cuTime);
    Log.e("Choosed Date and Time", date +" "+ time);
    String currentEpoch = converttoEpoch(context,cuDate, convertTo24Hour(cuTime));
    String choosedEpoch = converttoEpoch(context,choosedDate, convertTo24Hour(choosedTime));

    Log.e("Current Epoch",currentEpoch);
    Log.e("Choosed Epoch",choosedEpoch);
    if (Long.parseLong(choosedEpoch)>= Long.parseLong(currentEpoch)){
        //return true if choosedEpoch is greater
        return true;
    }else {
        //return false if ChoosedEpoch is less
        return false;
    }
}


    public static boolean Compare2DateTimes(Context context, String firstDate, String firstTime, String secondDate, String secondTime){


        Log.e("Current Date and Time", firstDate +" "+ firstTime);
        Log.e("Choosed Date and Time", secondDate +" "+ secondTime);
        String firstEpoch = converttoEpoch(context,firstDate, convertTo24Hour(firstTime));
        String secondEpoch = converttoEpoch(context,secondDate, convertTo24Hour(secondTime));

        Log.e("First Epoch",firstEpoch);
        Log.e("Second Epoch",secondEpoch);
        if (Long.parseLong(secondEpoch)> Long.parseLong(firstEpoch)){
            //return true if choosedEpoch is greater
            return true;
        }else {
            //return false if ChoosedEpoch is less
            return false;
        }
    }

    public static String convertTo24Hour(String time) {
        String tm = time;
        String[] str=time.split(":");
        String hour=str[0].trim();
        String min=str[1].trim();

        if (Integer.parseInt(hour)>12){
            return time;
        }else {
            if (Integer.parseInt(hour) < 12 && Integer.parseInt(min) > 8)
                tm = tm + " am";
            else
                tm = tm + " pm";

            java.text.DateFormat f1 = new SimpleDateFormat("hh:mm a"); //11:00 pm
            Date d = null;
            try {
                d = f1.parse(tm);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            java.text.DateFormat f2 = new SimpleDateFormat("HH:mm");
            String x = f2.format(d); // "23:00"

            return x;
        }

    }
}
