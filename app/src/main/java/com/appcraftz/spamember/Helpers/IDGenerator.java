package com.appcraftz.spamember.Helpers;



import java.util.Random;

/**
 * Created by appcraftz on 1/9/17.
 */

public class IDGenerator {

    public static String generateID()
    {
        String string = "";
        Random r = new Random();
        int rand = r.nextInt(999 - 10) + 10;

        string = string + String.valueOf(System.currentTimeMillis()) + String.valueOf(rand);

        return string;
    }
}
