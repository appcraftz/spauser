package com.appcraftz.spamember.Helpers;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Calendar;

/**
 * Created by appcraftz on 03/05/17.
 */

@SuppressLint("ValidFragment")
public class TimePicker extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

    View view;

    @SuppressLint("ValidFragment")
    public TimePicker(View view)
    {
        this.view = view;
    }
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        Calendar calendar = Calendar.getInstance();


        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute =calendar.get(Calendar.MINUTE);


        return new TimePickerDialog(getActivity(),this,hour,minute,true);



    }

    @Override
    public void onTimeSet(android.widget.TimePicker view, int hourOfDay, int minute) {
        String time = (hourOfDay>9? String.valueOf(hourOfDay):("0"+ String.valueOf(hourOfDay)))+":"+ (minute>9? String.valueOf(minute):("0"+ String.valueOf(minute)));
        if(this.view instanceof EditText)
            ((EditText)this.view).setText(time);
        else
            ((TextView)this.view).setText(time);
    }
}
