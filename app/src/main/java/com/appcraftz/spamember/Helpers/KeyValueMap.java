package com.appcraftz.spamember.Helpers;

/**
 * Created by Parth on 10/8/2016.
 */

public class KeyValueMap {
    private String key;
    private String value;

    public KeyValueMap(String key, String value)
    {
        this.key = key;
        this.value = value;
    }
    public String getKey()
    {
        return key;
    }
    public String getValue()
    {
        return value;
    }
}
