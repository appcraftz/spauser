package com.appcraftz.spamember.Helpers;

import java.io.Serializable;

//import android.util.Log;

/**
 * Created by Parth on 10/14/2016.
 */

public class ToDoObserver implements Serializable {
/*
    Context context;
    AlarmManager alarmManager;
    PendingIntent _myPendingIntent;


    public ToDoObserver(Context context)
    {
        this.context = context;
    }


    public void removeObserver()
    {
        if (alarmManager!= null) {
            alarmManager.cancel(_myPendingIntent);
        }

    }
    public void setObserver(String date, String time)
    {


        String[] dateparts = date.split("-");
        String[] timeparts = time.split("-");
        int day = Integer.parseInt(dateparts[0]);
        int month = Integer.parseInt(dateparts[1]);
        int year = Integer.parseInt(dateparts[2]);

        int hour = Integer.parseInt(timeparts[0]);
        int minute = Integer.parseInt(timeparts[1]);

        Calendar myAlarmDate = Calendar.getInstance();
        myAlarmDate.setTimeInMillis(System.currentTimeMillis());
        //myAlarmDate.set(year, month, day, hour, minute, 0);

        if(DateFormat.is24HourFormat(context)){
            myAlarmDate.set(Calendar.HOUR_OF_DAY, hour);
        }
        else {
            myAlarmDate.set(Calendar.HOUR, hour);
        }

        myAlarmDate.set(Calendar.MINUTE, minute);
        myAlarmDate.set(Calendar.SECOND, 0);
        myAlarmDate.set(Calendar.MILLISECOND, 0);
        myAlarmDate.set(Calendar.DAY_OF_MONTH,day);
        myAlarmDate.set(Calendar.MONTH,month-1);
        myAlarmDate.set(Calendar.YEAR,year);


        alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent _myIntent = new Intent(context, AlarmReceiver.class);


        _myIntent.putExtra("MyMessage","HERE I AM PASSING THEPERTICULAR MESSAGE WHICH SHOULD BE SHOW ON RECEIVER OF ALARM");
        Random r = new Random();
        int rand = r.nextInt(999 - 10) + 10;
        _myPendingIntent = PendingIntent.getBroadcast(context, rand, _myIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.set(AlarmManager.RTC_WAKEUP, myAlarmDate.getTimeInMillis(),_myPendingIntent);
     //   Log.e("ALarm set for",String.valueOf(myAlarmDate.getTimeInMillis()));

       // setReminder(date,time,);

    }

    public void setReminder(String date, String time, String message)
    {


        String[] dateparts = date.split("-");
        String[] timeparts = time.split(":");
        int day = Integer.parseInt(dateparts[0]);
        int month = Integer.parseInt(dateparts[1]);
        int year = Integer.parseInt(dateparts[2]);

        int hour = Integer.parseInt(timeparts[0]);
        int minute = Integer.parseInt(timeparts[1]);

        Calendar myAlarmDate = Calendar.getInstance();
        myAlarmDate.setTimeInMillis(System.currentTimeMillis());
        //myAlarmDate.set(year, month, day, hour, minute, 0);

        if(DateFormat.is24HourFormat(context)){
            myAlarmDate.set(Calendar.HOUR_OF_DAY, hour);
        }
        else {
            myAlarmDate.set(Calendar.HOUR, hour);
        }

        myAlarmDate.set(Calendar.MINUTE, minute);
        myAlarmDate.set(Calendar.SECOND, 0);
        myAlarmDate.set(Calendar.MILLISECOND, 0);
        myAlarmDate.set(Calendar.DAY_OF_MONTH,day);
        myAlarmDate.set(Calendar.MONTH,month-1);
        myAlarmDate.set(Calendar.YEAR,year);


        int minutesBefore;
        try {
          //  minutesBefore = Integer.parseInt(SPHelper.getSP(context, "minutesbefore"));
            minutesBefore = 10;
        }catch (Exception ex)
        {
            ex.printStackTrace();
            minutesBefore = 10;
        }

        myAlarmDate.add(Calendar.MINUTE,-minutesBefore);

        alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Intent _myIntent = new Intent(context, ReminderReceiver.class);
        //_myIntent.putExtra("ToDoId",id );
        _myIntent.putExtra("type",message);
        //_myIntent.putExtra("todoName",todo.getName());
      //  String string = GlobalVariables.serverPrefix;
        Random r = new Random();
        int rand = r.nextInt(999 - 10) + 10;
      //  String intentIdString = String.valueOf(rand) + string;
        //int intentId = Integer.parseInt(intentIdString);
        _myIntent.putExtra("MyMessage","HERE I AM PASSING THEPERTICULAR MESSAGE WHICH SHOULD BE SHOW ON RECEIVER OF ALARM");
        _myPendingIntent = PendingIntent.getBroadcast(context, rand, _myIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.set(AlarmManager.RTC_WAKEUP, myAlarmDate.getTimeInMillis(),_myPendingIntent);
       // Log.e("ALarm set for",String.valueOf(myAlarmDate.getTimeInMillis()));

    }


    public void setMeetingReminder(String date, String time, RemainderItem remainderItem)
    {

       // Log.e("Date while setting","alarm is: "+date);
        //Log.e("Time while setting","alarm is: "+time);
        String[] dateparts = date.split("-");
        String[] timeparts = time.split(":");
        int day = Integer.parseInt(dateparts[0]);
        int month = Integer.parseInt(dateparts[1]);
        int year = Integer.parseInt(dateparts[2]);

       // Log.e("date Splitted","day"+day+"Month"+month+"year"+year);
        int hour = Integer.parseInt(timeparts[0]);
        int minute = Integer.parseInt(timeparts[1]);


        Calendar myAlarmDate = Calendar.getInstance();
        myAlarmDate.setTimeInMillis(System.currentTimeMillis());

        if(hour>12){
            myAlarmDate.set(Calendar.HOUR_OF_DAY, hour);
          //  Log.e("24Hour hour",""+hour);
        }
        else {
            String splitted[] = StringToEpoch.convertTo24Hour(time).split(":");
            myAlarmDate.set(Calendar.HOUR_OF_DAY,Integer.parseInt(splitted[0]));
          //  Log.e("24Hour hour",splitted[0]);
        }


        //myAlarmDate.set(year, month, day, hour, minute, 0);


//1512598920000
        myAlarmDate.set(Calendar.MINUTE, minute);
        myAlarmDate.set(Calendar.SECOND, 0);
        myAlarmDate.set(Calendar.MILLISECOND, 0);
        myAlarmDate.set(Calendar.DAY_OF_MONTH,day);
        myAlarmDate.set(Calendar.MONTH,month-1);
        myAlarmDate.set(Calendar.YEAR,year);
       // Log.e("Setting Time in Alarm",String.valueOf(myAlarmDate.get(Calendar.HOUR_OF_DAY) +":"+String.valueOf(myAlarmDate.get(Calendar.MINUTE))));
       // Log.e("Setting Date in Alarm",String.valueOf(myAlarmDate.get(Calendar.DAY_OF_MONTH) +"-"+String.valueOf(myAlarmDate.get(Calendar.MONTH)+"-"+String.valueOf(myAlarmDate.get(Calendar.YEAR)))));

        int minutesBefore;
        try {
            minutesBefore = Integer.parseInt(SPHelper.getSP(context, "minutesbefore"));
        }catch (Exception ex)
        {
            ex.printStackTrace();
            minutesBefore = 10;
        }

        myAlarmDate.add(Calendar.MINUTE,-minutesBefore);






        alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Intent _myIntent = new Intent(context, ReminderReceiver.class);
        //_myIntent.putExtra("ToDoId",id );
        Bundle bundle = new Bundle();
        bundle.putSerializable("reminder",remainderItem);
        _myIntent.putExtras(bundle);
      //  _myIntent.putExtra("todoName",message);

       // String string = GlobalVariables.serverPrefix;
        Random r = new Random();
        int rand = r.nextInt(999 - 10) + 10;
        //  String intentIdString = String.valueOf(rand) + string;
        //int intentId = Integer.parseInt(intentIdString);
        _myIntent.putExtra("MyMessage","HERE I AM PASSING THEPERTICULAR MESSAGE WHICH SHOULD BE SHOW ON RECEIVER OF ALARM");
        _myPendingIntent = PendingIntent.getBroadcast(context, rand, _myIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.set(AlarmManager.RTC_WAKEUP, myAlarmDate.getTimeInMillis(),_myPendingIntent);
       // Log.e("ALarm set for",String.valueOf(myAlarmDate.getTimeInMillis()));

    }

*/
}
