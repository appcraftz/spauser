package com.appcraftz.spamember.Helpers;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.appcraftz.spamember.R;

import java.util.Calendar;

/**
 * Created by appcraftz on 03/05/17.
 */

@SuppressLint("ValidFragment")
public class DatePicker extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    public DateSet dateSet = null;

    View view;



    public DatePicker(View view)
    {

        this.view = view;
    }
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        Calendar calendar = Calendar.getInstance();

        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);


        return new DatePickerDialog(getActivity(), R.style.DialogTheme,this,year,month,day);






    }

    @Override
    public void onDateSet(android.widget.DatePicker view, int year, int month, int dayOfMonth) {


        String date = String.valueOf(dayOfMonth)+"-"+ String.valueOf(month+1)+"-"+ String.valueOf(year);
        if(this.view instanceof EditText) {
            ((EditText) this.view).setText(date);

        }
        else{
            ((TextView)this.view).setText(date);
            }
        if (dateSet!=null){
            dateSet.    onDateSet(date);
    }}


    public void setOnDateSetClickedListener(DateSet dateSet){
        this.dateSet=dateSet;
    }
}
