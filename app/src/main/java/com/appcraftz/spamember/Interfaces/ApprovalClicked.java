package com.appcraftz.spamember.Interfaces;

import com.appcraftz.spamember.Items.MemberItem;

/**
 * Created by appcraftz on 29/3/18.
 */

public interface ApprovalClicked {
    public void onApprovalClicked(MemberItem memberItem,boolean isApproved);
}
