package com.appcraftz.spamember.Interfaces;


import com.appcraftz.spamember.Items.OfferItem;

public interface DeleteOffer {
    public void onDeleteOfferClicked(OfferItem offerItem);
}
