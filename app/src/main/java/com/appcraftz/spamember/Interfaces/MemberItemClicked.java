package com.appcraftz.spamember.Interfaces;

import com.appcraftz.spamember.Items.MemberItem;

/**
 * Created by appcraftz on 20/3/18.
 */

public interface MemberItemClicked {

    public void onMemberItemClicked(MemberItem memberItem);
}
